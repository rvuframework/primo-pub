//===-- X86MCCodeEmitter.cpp - Convert X86 code to machine code -----------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements the X86MCCodeEmitter class.
//
//===----------------------------------------------------------------------===//

#include "MCTargetDesc/X86MCTargetDesc.h"
#include "MCTargetDesc/X86BaseInfo.h"
#include "MCTargetDesc/X86FixupKinds.h"
#include "llvm/MC/MCCodeEmitter.h"
#include "llvm/MC/MCContext.h"
#include "llvm/MC/MCExpr.h"
#include "llvm/MC/MCInst.h"
#include "llvm/MC/MCInstrInfo.h"
#include "llvm/MC/MCRegisterInfo.h"
#include "llvm/MC/MCSubtargetInfo.h"
#include "llvm/MC/MCSymbol.h"
#include "llvm/Support/raw_ostream.h"
#include <string>
#include <cstring>
#define MAXSPACE 40
using namespace std;

using namespace llvm;

#define DEBUG_TYPE "mccodeemitter"

namespace {
class X86MCCodeEmitter : public MCCodeEmitter {
  X86MCCodeEmitter(const X86MCCodeEmitter &) = delete;
  void operator=(const X86MCCodeEmitter &) = delete;
  const MCInstrInfo &MCII;
  MCContext &Ctx;
public:
  X86MCCodeEmitter(const MCInstrInfo &mcii, MCContext &ctx)
: MCII(mcii), Ctx(ctx) {
  }
  
  
  
  

  ~X86MCCodeEmitter() override {}
  
  
  


  bool is64BitMode(const MCSubtargetInfo &STI) const {
    return STI.getFeatureBits()[X86::Mode64Bit];
  }

  bool is32BitMode(const MCSubtargetInfo &STI) const {
    return STI.getFeatureBits()[X86::Mode32Bit];
  }

  bool is16BitMode(const MCSubtargetInfo &STI) const {
    return STI.getFeatureBits()[X86::Mode16Bit];
  }

  /// Is16BitMemOperand - Return true if the specified instruction has
  /// a 16-bit memory operand. Op specifies the operand # of the memoperand.
  bool Is16BitMemOperand(const MCInst &MI, unsigned Op,
                         const MCSubtargetInfo &STI) const {
    const MCOperand &BaseReg  = MI.getOperand(Op+X86::AddrBaseReg);
    const MCOperand &IndexReg = MI.getOperand(Op+X86::AddrIndexReg);
    const MCOperand &Disp     = MI.getOperand(Op+X86::AddrDisp);

    if (is16BitMode(STI) && BaseReg.getReg() == 0 &&
        Disp.isImm() && Disp.getImm() < 0x10000)
      return true;
    if ((BaseReg.getReg() != 0 &&
         X86MCRegisterClasses[X86::GR16RegClassID].contains(BaseReg.getReg())) ||
        (IndexReg.getReg() != 0 &&
         X86MCRegisterClasses[X86::GR16RegClassID].contains(IndexReg.getReg())))
      return true;
    return false;
  }

  unsigned GetX86RegNum(const MCOperand &MO) const {
    return Ctx.getRegisterInfo()->getEncodingValue(MO.getReg()) & 0x7;
  }

  unsigned getX86RegEncoding(const MCInst &MI, unsigned OpNum) const {
    return Ctx.getRegisterInfo()->getEncodingValue(
                                                 MI.getOperand(OpNum).getReg());
  }

  // Does this register require a bit to be set in REX prefix.
  bool isREXExtendedReg(const MCInst &MI, unsigned OpNum) const {
    return (getX86RegEncoding(MI, OpNum) >> 3) & 1;
  }

  void EmitByte(uint8_t C, unsigned &CurByte, raw_ostream &OS) const {
    OS << (char)C;
    ++CurByte;
  }

  void EmitConstant(uint64_t Val, unsigned Size, unsigned &CurByte,
                    raw_ostream &OS) const {
    // Output the constant in little endian byte order.
    for (unsigned i = 0; i != Size; ++i) {
      EmitByte(Val & 255, CurByte, OS);
      Val >>= 8;
    }
  }

  void EmitImmediate(const MCOperand &Disp, SMLoc Loc,
                     unsigned ImmSize, MCFixupKind FixupKind,
                     unsigned &CurByte, raw_ostream &OS,
                     SmallVectorImpl<MCFixup> &Fixups,
                     int ImmOffset = 0) const;

  inline static uint8_t ModRMByte(unsigned Mod, unsigned RegOpcode,
                                  unsigned RM) {
    assert(Mod < 4 && RegOpcode < 8 && RM < 8 && "ModRM Fields out of range!");
    return RM | (RegOpcode << 3) | (Mod << 6);
  }

  void EmitRegModRMByte(const MCOperand &ModRMReg, unsigned RegOpcodeFld,
                        unsigned &CurByte, raw_ostream &OS) const {
    EmitByte(ModRMByte(3, RegOpcodeFld, GetX86RegNum(ModRMReg)), CurByte, OS);
  }

  void EmitSIBByte(unsigned SS, unsigned Index, unsigned Base,
                   unsigned &CurByte, raw_ostream &OS) const {
    // SIB byte is in the same format as the ModRMByte.
    EmitByte(ModRMByte(SS, Index, Base), CurByte, OS);
  }

  void emitMemModRMByte(const MCInst &MI, unsigned Op, unsigned RegOpcodeField,
                        uint64_t TSFlags, bool Rex, unsigned &CurByte,
                        raw_ostream &OS, SmallVectorImpl<MCFixup> &Fixups,
                        const MCSubtargetInfo &STI) const;

  void encodeInstruction(const MCInst &MI, raw_ostream &OS,
                         SmallVectorImpl<MCFixup> &Fixups,
                         const MCSubtargetInfo &STI) const override;
  
  
 void encodePIM(const MCInst &MI, raw_ostream &OS,
                         SmallVectorImpl<MCFixup> &Fixups,
                         const MCSubtargetInfo &STI) const; 
  
  

  void EmitVEXOpcodePrefix(uint64_t TSFlags, unsigned &CurByte, int MemOperand,
                           const MCInst &MI, const MCInstrDesc &Desc,
                           raw_ostream &OS) const;

  void EmitSegmentOverridePrefix(unsigned &CurByte, unsigned SegOperand,
                                 const MCInst &MI, raw_ostream &OS) const;

  bool emitOpcodePrefix(uint64_t TSFlags, unsigned &CurByte, int MemOperand,
                        const MCInst &MI, const MCInstrDesc &Desc,
                        const MCSubtargetInfo &STI, raw_ostream &OS) const;

  uint8_t DetermineREXPrefix(const MCInst &MI, uint64_t TSFlags,
                             int MemOperand, const MCInstrDesc &Desc) const;
};

} // end anonymous namespace

MCCodeEmitter *llvm::createX86MCCodeEmitter(const MCInstrInfo &MCII,
                                            const MCRegisterInfo &MRI,
                                            MCContext &Ctx) {
  return new X86MCCodeEmitter(MCII, Ctx);
}

/// isDisp8 - Return true if this signed displacement fits in a 8-bit
/// sign-extended field.
static bool isDisp8(int Value) {
  return Value == (int8_t)Value;
}

/// isCDisp8 - Return true if this signed displacement fits in a 8-bit
/// compressed dispacement field.
static bool isCDisp8(uint64_t TSFlags, int Value, int& CValue) {
  assert(((TSFlags & X86II::EncodingMask) == X86II::EVEX) &&
         "Compressed 8-bit displacement is only valid for EVEX inst.");

  unsigned CD8_Scale =
    (TSFlags & X86II::CD8_Scale_Mask) >> X86II::CD8_Scale_Shift;
  if (CD8_Scale == 0) {
    CValue = Value;
    return isDisp8(Value);
  }

  unsigned Mask = CD8_Scale - 1;
  assert((CD8_Scale & Mask) == 0 && "Invalid memory object size.");
  if (Value & Mask) // Unaligned offset
    return false;
  Value /= (int)CD8_Scale;
  bool Ret = (Value == (int8_t)Value);

  if (Ret)
    CValue = Value;
  return Ret;
}

/// getImmFixupKind - Return the appropriate fixup kind to use for an immediate
/// in an instruction with the specified TSFlags.
static MCFixupKind getImmFixupKind(uint64_t TSFlags) {
  unsigned Size = X86II::getSizeOfImm(TSFlags);
  bool isPCRel = X86II::isImmPCRel(TSFlags);

  if (X86II::isImmSigned(TSFlags)) {
    switch (Size) {
    default: llvm_unreachable("Unsupported signed fixup size!");
    case 4: return MCFixupKind(X86::reloc_signed_4byte);
    }
  }
  return MCFixup::getKindForSize(Size, isPCRel);
}

/// Is32BitMemOperand - Return true if the specified instruction has
/// a 32-bit memory operand. Op specifies the operand # of the memoperand.
static bool Is32BitMemOperand(const MCInst &MI, unsigned Op) {
  const MCOperand &BaseReg  = MI.getOperand(Op+X86::AddrBaseReg);
  const MCOperand &IndexReg = MI.getOperand(Op+X86::AddrIndexReg);

  if ((BaseReg.getReg() != 0 &&
       X86MCRegisterClasses[X86::GR32RegClassID].contains(BaseReg.getReg())) ||
      (IndexReg.getReg() != 0 &&
       X86MCRegisterClasses[X86::GR32RegClassID].contains(IndexReg.getReg())))
    return true;
  if (BaseReg.getReg() == X86::EIP) {
    assert(IndexReg.getReg() == 0 && "Invalid eip-based address.");
    return true;
  }
  return false;
}

/// Is64BitMemOperand - Return true if the specified instruction has
/// a 64-bit memory operand. Op specifies the operand # of the memoperand.
#ifndef NDEBUG
static bool Is64BitMemOperand(const MCInst &MI, unsigned Op) {
  const MCOperand &BaseReg  = MI.getOperand(Op+X86::AddrBaseReg);
  const MCOperand &IndexReg = MI.getOperand(Op+X86::AddrIndexReg);

  if ((BaseReg.getReg() != 0 &&
       X86MCRegisterClasses[X86::GR64RegClassID].contains(BaseReg.getReg())) ||
      (IndexReg.getReg() != 0 &&
       X86MCRegisterClasses[X86::GR64RegClassID].contains(IndexReg.getReg())))
    return true;
  return false;
}
#endif

/// StartsWithGlobalOffsetTable - Check if this expression starts with
///  _GLOBAL_OFFSET_TABLE_ and if it is of the form
///  _GLOBAL_OFFSET_TABLE_-symbol. This is needed to support PIC on ELF
/// i386 as _GLOBAL_OFFSET_TABLE_ is magical. We check only simple case that
/// are know to be used: _GLOBAL_OFFSET_TABLE_ by itself or at the start
/// of a binary expression.
enum GlobalOffsetTableExprKind {
  GOT_None,
  GOT_Normal,
  GOT_SymDiff
};
static GlobalOffsetTableExprKind
StartsWithGlobalOffsetTable(const MCExpr *Expr) {
  const MCExpr *RHS = nullptr;
  if (Expr->getKind() == MCExpr::Binary) {
    const MCBinaryExpr *BE = static_cast<const MCBinaryExpr *>(Expr);
    Expr = BE->getLHS();
    RHS = BE->getRHS();
  }

  if (Expr->getKind() != MCExpr::SymbolRef)
    return GOT_None;

  const MCSymbolRefExpr *Ref = static_cast<const MCSymbolRefExpr*>(Expr);
  const MCSymbol &S = Ref->getSymbol();
  if (S.getName() != "_GLOBAL_OFFSET_TABLE_")
    return GOT_None;
  if (RHS && RHS->getKind() == MCExpr::SymbolRef)
    return GOT_SymDiff;
  return GOT_Normal;
}

static bool HasSecRelSymbolRef(const MCExpr *Expr) {
  if (Expr->getKind() == MCExpr::SymbolRef) {
    const MCSymbolRefExpr *Ref = static_cast<const MCSymbolRefExpr*>(Expr);
    return Ref->getKind() == MCSymbolRefExpr::VK_SECREL;
  }
  return false;
}

void X86MCCodeEmitter::
EmitImmediate(const MCOperand &DispOp, SMLoc Loc, unsigned Size,
              MCFixupKind FixupKind, unsigned &CurByte, raw_ostream &OS,
              SmallVectorImpl<MCFixup> &Fixups, int ImmOffset) const {
  const MCExpr *Expr = nullptr;
  if (DispOp.isImm()) {
    // If this is a simple integer displacement that doesn't require a
    // relocation, emit it now.
    if (FixupKind != FK_PCRel_1 &&
        FixupKind != FK_PCRel_2 &&
        FixupKind != FK_PCRel_4) {
      EmitConstant(DispOp.getImm()+ImmOffset, Size, CurByte, OS);
      return;
    }
    Expr = MCConstantExpr::create(DispOp.getImm(), Ctx);
  } else {
    Expr = DispOp.getExpr();
  }

  // If we have an immoffset, add it to the expression.
  if ((FixupKind == FK_Data_4 ||
       FixupKind == FK_Data_8 ||
       FixupKind == MCFixupKind(X86::reloc_signed_4byte))) {
    GlobalOffsetTableExprKind Kind = StartsWithGlobalOffsetTable(Expr);
    if (Kind != GOT_None) {
      assert(ImmOffset == 0);

      if (Size == 8) {
        FixupKind = MCFixupKind(X86::reloc_global_offset_table8);
      } else {
        assert(Size == 4);
        FixupKind = MCFixupKind(X86::reloc_global_offset_table);
      }

      if (Kind == GOT_Normal)
        ImmOffset = CurByte;
    } else if (Expr->getKind() == MCExpr::SymbolRef) {
      if (HasSecRelSymbolRef(Expr)) {
        FixupKind = MCFixupKind(FK_SecRel_4);
      }
    } else if (Expr->getKind() == MCExpr::Binary) {
      const MCBinaryExpr *Bin = static_cast<const MCBinaryExpr*>(Expr);
      if (HasSecRelSymbolRef(Bin->getLHS())
          || HasSecRelSymbolRef(Bin->getRHS())) {
        FixupKind = MCFixupKind(FK_SecRel_4);
      }
    }
  }

  // If the fixup is pc-relative, we need to bias the value to be relative to
  // the start of the field, not the end of the field.
  if (FixupKind == FK_PCRel_4 ||
      FixupKind == MCFixupKind(X86::reloc_riprel_4byte) ||
      FixupKind == MCFixupKind(X86::reloc_riprel_4byte_movq_load) ||
      FixupKind == MCFixupKind(X86::reloc_riprel_4byte_relax) ||
      FixupKind == MCFixupKind(X86::reloc_riprel_4byte_relax_rex))
    ImmOffset -= 4;
  if (FixupKind == FK_PCRel_2)
    ImmOffset -= 2;
  if (FixupKind == FK_PCRel_1)
    ImmOffset -= 1;

  if (ImmOffset)
    Expr = MCBinaryExpr::createAdd(Expr, MCConstantExpr::create(ImmOffset, Ctx),
                                   Ctx);

  // Emit a symbolic constant as a fixup and 4 zeros.
  Fixups.push_back(MCFixup::create(CurByte, Expr, FixupKind, Loc));
  EmitConstant(0, Size, CurByte, OS);
}

void X86MCCodeEmitter::emitMemModRMByte(const MCInst &MI, unsigned Op,
                                        unsigned RegOpcodeField,
                                        uint64_t TSFlags, bool Rex,
                                        unsigned &CurByte, raw_ostream &OS,
                                        SmallVectorImpl<MCFixup> &Fixups,
                                        const MCSubtargetInfo &STI) const {
  const MCOperand &Disp     = MI.getOperand(Op+X86::AddrDisp);
  const MCOperand &Base     = MI.getOperand(Op+X86::AddrBaseReg);
  const MCOperand &Scale    = MI.getOperand(Op+X86::AddrScaleAmt);
  const MCOperand &IndexReg = MI.getOperand(Op+X86::AddrIndexReg);
  unsigned BaseReg = Base.getReg();
  bool HasEVEX = (TSFlags & X86II::EncodingMask) == X86II::EVEX;

  // Handle %rip relative addressing.
  if (BaseReg == X86::RIP ||
      BaseReg == X86::EIP) {    // [disp32+rIP] in X86-64 mode
    assert(is64BitMode(STI) && "Rip-relative addressing requires 64-bit mode");
    assert(IndexReg.getReg() == 0 && "Invalid rip-relative address");
    EmitByte(ModRMByte(0, RegOpcodeField, 5), CurByte, OS);

    unsigned Opcode = MI.getOpcode();
    // movq loads are handled with a special relocation form which allows the
    // linker to eliminate some loads for GOT references which end up in the
    // same linkage unit.
    unsigned FixupKind = [=]() {
      switch (Opcode) {
      default:
        return X86::reloc_riprel_4byte;
      case X86::MOV64rm:
        assert(Rex);
        return X86::reloc_riprel_4byte_movq_load;
      case X86::CALL64m:
      case X86::JMP64m:
      case X86::TEST64rm:
      case X86::ADC64rm:
      case X86::ADD64rm:
      case X86::AND64rm:
      case X86::CMP64rm:
      case X86::OR64rm:
      case X86::SBB64rm:
      case X86::SUB64rm:
      case X86::XOR64rm:
        return Rex ? X86::reloc_riprel_4byte_relax_rex
                   : X86::reloc_riprel_4byte_relax;
      }
    }();

    // rip-relative addressing is actually relative to the *next* instruction.
    // Since an immediate can follow the mod/rm byte for an instruction, this
    // means that we need to bias the immediate field of the instruction with
    // the size of the immediate field.  If we have this case, add it into the
    // expression to emit.
    int ImmSize = X86II::hasImm(TSFlags) ? X86II::getSizeOfImm(TSFlags) : 0;

    EmitImmediate(Disp, MI.getLoc(), 4, MCFixupKind(FixupKind),
                  CurByte, OS, Fixups, -ImmSize);
    return;
  }

  unsigned BaseRegNo = BaseReg ? GetX86RegNum(Base) : -1U;

  // 16-bit addressing forms of the ModR/M byte have a different encoding for
  // the R/M field and are far more limited in which registers can be used.
  if (Is16BitMemOperand(MI, Op, STI)) {
    if (BaseReg) {
      // For 32-bit addressing, the row and column values in Table 2-2 are
      // basically the same. It's AX/CX/DX/BX/SP/BP/SI/DI in that order, with
      // some special cases. And GetX86RegNum reflects that numbering.
      // For 16-bit addressing it's more fun, as shown in the SDM Vol 2A,
      // Table 2-1 "16-Bit Addressing Forms with the ModR/M byte". We can only
      // use SI/DI/BP/BX, which have "row" values 4-7 in no particular order,
      // while values 0-3 indicate the allowed combinations (base+index) of
      // those: 0 for BX+SI, 1 for BX+DI, 2 for BP+SI, 3 for BP+DI.
      //
      // R16Table[] is a lookup from the normal RegNo, to the row values from
      // Table 2-1 for 16-bit addressing modes. Where zero means disallowed.
      static const unsigned R16Table[] = { 0, 0, 0, 7, 0, 6, 4, 5 };
      unsigned RMfield = R16Table[BaseRegNo];

      assert(RMfield && "invalid 16-bit base register");

      if (IndexReg.getReg()) {
        unsigned IndexReg16 = R16Table[GetX86RegNum(IndexReg)];

        assert(IndexReg16 && "invalid 16-bit index register");
        // We must have one of SI/DI (4,5), and one of BP/BX (6,7).
        assert(((IndexReg16 ^ RMfield) & 2) &&
               "invalid 16-bit base/index register combination");
        assert(Scale.getImm() == 1 &&
               "invalid scale for 16-bit memory reference");

        // Allow base/index to appear in either order (although GAS doesn't).
        if (IndexReg16 & 2)
          RMfield = (RMfield & 1) | ((7 - IndexReg16) << 1);
        else
          RMfield = (IndexReg16 & 1) | ((7 - RMfield) << 1);
      }

      if (Disp.isImm() && isDisp8(Disp.getImm())) {
        if (Disp.getImm() == 0 && BaseRegNo != N86::EBP) {
          // There is no displacement; just the register.
          EmitByte(ModRMByte(0, RegOpcodeField, RMfield), CurByte, OS);
          return;
        }
        // Use the [REG]+disp8 form, including for [BP] which cannot be encoded.
        EmitByte(ModRMByte(1, RegOpcodeField, RMfield), CurByte, OS);
        EmitImmediate(Disp, MI.getLoc(), 1, FK_Data_1, CurByte, OS, Fixups);
        return;
      }
      // This is the [REG]+disp16 case.
      EmitByte(ModRMByte(2, RegOpcodeField, RMfield), CurByte, OS);
    } else {
      // There is no BaseReg; this is the plain [disp16] case.
      EmitByte(ModRMByte(0, RegOpcodeField, 6), CurByte, OS);
    }

    // Emit 16-bit displacement for plain disp16 or [REG]+disp16 cases.
    EmitImmediate(Disp, MI.getLoc(), 2, FK_Data_2, CurByte, OS, Fixups);
    return;
  }

  // Determine whether a SIB byte is needed.
  // If no BaseReg, issue a RIP relative instruction only if the MCE can
  // resolve addresses on-the-fly, otherwise use SIB (Intel Manual 2A, table
  // 2-7) and absolute references.

  if (// The SIB byte must be used if there is an index register.
      IndexReg.getReg() == 0 &&
      // The SIB byte must be used if the base is ESP/RSP/R12, all of which
      // encode to an R/M value of 4, which indicates that a SIB byte is
      // present.
      BaseRegNo != N86::ESP &&
      // If there is no base register and we're in 64-bit mode, we need a SIB
      // byte to emit an addr that is just 'disp32' (the non-RIP relative form).
      (!is64BitMode(STI) || BaseReg != 0)) {

    if (BaseReg == 0) {          // [disp32]     in X86-32 mode
      EmitByte(ModRMByte(0, RegOpcodeField, 5), CurByte, OS);
      EmitImmediate(Disp, MI.getLoc(), 4, FK_Data_4, CurByte, OS, Fixups);
      return;
    }

    // If the base is not EBP/ESP and there is no displacement, use simple
    // indirect register encoding, this handles addresses like [EAX].  The
    // encoding for [EBP] with no displacement means [disp32] so we handle it
    // by emitting a displacement of 0 below.
    if (Disp.isImm() && Disp.getImm() == 0 && BaseRegNo != N86::EBP) {
      EmitByte(ModRMByte(0, RegOpcodeField, BaseRegNo), CurByte, OS);
      return;
    }

    // Otherwise, if the displacement fits in a byte, encode as [REG+disp8].
    if (Disp.isImm()) {
      if (!HasEVEX && isDisp8(Disp.getImm())) {
        EmitByte(ModRMByte(1, RegOpcodeField, BaseRegNo), CurByte, OS);
        EmitImmediate(Disp, MI.getLoc(), 1, FK_Data_1, CurByte, OS, Fixups);
        return;
      }
      // Try EVEX compressed 8-bit displacement first; if failed, fall back to
      // 32-bit displacement.
      int CDisp8 = 0;
      if (HasEVEX && isCDisp8(TSFlags, Disp.getImm(), CDisp8)) {
        EmitByte(ModRMByte(1, RegOpcodeField, BaseRegNo), CurByte, OS);
        EmitImmediate(Disp, MI.getLoc(), 1, FK_Data_1, CurByte, OS, Fixups,
                      CDisp8 - Disp.getImm());
        return;
      }
    }

    // Otherwise, emit the most general non-SIB encoding: [REG+disp32]
    EmitByte(ModRMByte(2, RegOpcodeField, BaseRegNo), CurByte, OS);
    unsigned Opcode = MI.getOpcode();
    unsigned FixupKind = Opcode == X86::MOV32rm ? X86::reloc_signed_4byte_relax
                                                : X86::reloc_signed_4byte;
    EmitImmediate(Disp, MI.getLoc(), 4, MCFixupKind(FixupKind), CurByte, OS,
                  Fixups);
    return;
  }

  // We need a SIB byte, so start by outputting the ModR/M byte first
  assert(IndexReg.getReg() != X86::ESP &&
         IndexReg.getReg() != X86::RSP && "Cannot use ESP as index reg!");

  bool ForceDisp32 = false;
  bool ForceDisp8  = false;
  int CDisp8 = 0;
  int ImmOffset = 0;
  if (BaseReg == 0) {
    // If there is no base register, we emit the special case SIB byte with
    // MOD=0, BASE=5, to JUST get the index, scale, and displacement.
    EmitByte(ModRMByte(0, RegOpcodeField, 4), CurByte, OS);
    ForceDisp32 = true;
  } else if (!Disp.isImm()) {
    // Emit the normal disp32 encoding.
    EmitByte(ModRMByte(2, RegOpcodeField, 4), CurByte, OS);
    ForceDisp32 = true;
  } else if (Disp.getImm() == 0 &&
             // Base reg can't be anything that ends up with '5' as the base
             // reg, it is the magic [*] nomenclature that indicates no base.
             BaseRegNo != N86::EBP) {
    // Emit no displacement ModR/M byte
    EmitByte(ModRMByte(0, RegOpcodeField, 4), CurByte, OS);
  } else if (!HasEVEX && isDisp8(Disp.getImm())) {
    // Emit the disp8 encoding.
    EmitByte(ModRMByte(1, RegOpcodeField, 4), CurByte, OS);
    ForceDisp8 = true;           // Make sure to force 8 bit disp if Base=EBP
  } else if (HasEVEX && isCDisp8(TSFlags, Disp.getImm(), CDisp8)) {
    // Emit the disp8 encoding.
    EmitByte(ModRMByte(1, RegOpcodeField, 4), CurByte, OS);
    ForceDisp8 = true;           // Make sure to force 8 bit disp if Base=EBP
    ImmOffset = CDisp8 - Disp.getImm();
  } else {
    // Emit the normal disp32 encoding.
    EmitByte(ModRMByte(2, RegOpcodeField, 4), CurByte, OS);
  }

  // Calculate what the SS field value should be...
  static const unsigned SSTable[] = { ~0U, 0, 1, ~0U, 2, ~0U, ~0U, ~0U, 3 };
  unsigned SS = SSTable[Scale.getImm()];

  if (BaseReg == 0) {
    // Handle the SIB byte for the case where there is no base, see Intel
    // Manual 2A, table 2-7. The displacement has already been output.
    unsigned IndexRegNo;
    if (IndexReg.getReg())
      IndexRegNo = GetX86RegNum(IndexReg);
    else // Examples: [ESP+1*<noreg>+4] or [scaled idx]+disp32 (MOD=0,BASE=5)
      IndexRegNo = 4;
    EmitSIBByte(SS, IndexRegNo, 5, CurByte, OS);
  } else {
    unsigned IndexRegNo;
    if (IndexReg.getReg())
      IndexRegNo = GetX86RegNum(IndexReg);
    else
      IndexRegNo = 4;   // For example [ESP+1*<noreg>+4]
    EmitSIBByte(SS, IndexRegNo, GetX86RegNum(Base), CurByte, OS);
  }

  // Do we need to output a displacement?
  if (ForceDisp8)
    EmitImmediate(Disp, MI.getLoc(), 1, FK_Data_1, CurByte, OS, Fixups, ImmOffset);
  else if (ForceDisp32 || Disp.getImm() != 0)
    EmitImmediate(Disp, MI.getLoc(), 4, MCFixupKind(X86::reloc_signed_4byte),
                  CurByte, OS, Fixups);
}

/// EmitVEXOpcodePrefix - AVX instructions are encoded using a opcode prefix
/// called VEX.
void X86MCCodeEmitter::EmitVEXOpcodePrefix(uint64_t TSFlags, unsigned &CurByte,
                                           int MemOperand, const MCInst &MI,
                                           const MCInstrDesc &Desc,
                                           raw_ostream &OS) const {
  assert(!(TSFlags & X86II::LOCK) && "Can't have LOCK VEX.");

  uint64_t Encoding = TSFlags & X86II::EncodingMask;
  bool HasEVEX_K = TSFlags & X86II::EVEX_K;
  bool HasVEX_4V = TSFlags & X86II::VEX_4V;
  bool HasEVEX_RC = TSFlags & X86II::EVEX_RC;

  // VEX_R: opcode externsion equivalent to REX.R in
  // 1's complement (inverted) form
  //
  //  1: Same as REX_R=0 (must be 1 in 32-bit mode)
  //  0: Same as REX_R=1 (64 bit mode only)
  //
  uint8_t VEX_R = 0x1;
  uint8_t EVEX_R2 = 0x1;

  // VEX_X: equivalent to REX.X, only used when a
  // register is used for index in SIB Byte.
  //
  //  1: Same as REX.X=0 (must be 1 in 32-bit mode)
  //  0: Same as REX.X=1 (64-bit mode only)
  uint8_t VEX_X = 0x1;

  // VEX_B:
  //
  //  1: Same as REX_B=0 (ignored in 32-bit mode)
  //  0: Same as REX_B=1 (64 bit mode only)
  //
  uint8_t VEX_B = 0x1;

  // VEX_W: opcode specific (use like REX.W, or used for
  // opcode extension, or ignored, depending on the opcode byte)
  uint8_t VEX_W = (TSFlags & X86II::VEX_W) ? 1 : 0;

  // VEX_5M (VEX m-mmmmm field):
  //
  //  0b00000: Reserved for future use
  //  0b00001: implied 0F leading opcode
  //  0b00010: implied 0F 38 leading opcode bytes
  //  0b00011: implied 0F 3A leading opcode bytes
  //  0b00100-0b11111: Reserved for future use
  //  0b01000: XOP map select - 08h instructions with imm byte
  //  0b01001: XOP map select - 09h instructions with no imm byte
  //  0b01010: XOP map select - 0Ah instructions with imm dword
  uint8_t VEX_5M;
  switch (TSFlags & X86II::OpMapMask) {
  default: llvm_unreachable("Invalid prefix!");
  case X86II::TB:   VEX_5M = 0x1; break; // 0F
  case X86II::T8:   VEX_5M = 0x2; break; // 0F 38
  case X86II::TA:   VEX_5M = 0x3; break; // 0F 3A
  case X86II::XOP8: VEX_5M = 0x8; break;
  case X86II::XOP9: VEX_5M = 0x9; break;
  case X86II::XOPA: VEX_5M = 0xA; break;
  }

  // VEX_4V (VEX vvvv field): a register specifier
  // (in 1's complement form) or 1111 if unused.
  uint8_t VEX_4V = 0xf;
  uint8_t EVEX_V2 = 0x1;

  // EVEX_L2/VEX_L (Vector Length):
  //
  // L2 L
  //  0 0: scalar or 128-bit vector
  //  0 1: 256-bit vector
  //  1 0: 512-bit vector
  //
  uint8_t VEX_L = (TSFlags & X86II::VEX_L) ? 1 : 0;
  uint8_t EVEX_L2 = (TSFlags & X86II::EVEX_L2) ? 1 : 0;

  // VEX_PP: opcode extension providing equivalent
  // functionality of a SIMD prefix
  //
  //  0b00: None
  //  0b01: 66
  //  0b10: F3
  //  0b11: F2
  //
  uint8_t VEX_PP;
  switch (TSFlags & X86II::OpPrefixMask) {
  default: llvm_unreachable("Invalid op prefix!");
  case X86II::PS: VEX_PP = 0x0; break; // none
  case X86II::PD: VEX_PP = 0x1; break; // 66
  case X86II::XS: VEX_PP = 0x2; break; // F3
  case X86II::XD: VEX_PP = 0x3; break; // F2
  }

  // EVEX_U
  uint8_t EVEX_U = 1; // Always '1' so far

  // EVEX_z
  uint8_t EVEX_z = (HasEVEX_K && (TSFlags & X86II::EVEX_Z)) ? 1 : 0;

  // EVEX_b
  uint8_t EVEX_b = (TSFlags & X86II::EVEX_B) ? 1 : 0;

  // EVEX_rc
  uint8_t EVEX_rc = 0;

  // EVEX_aaa
  uint8_t EVEX_aaa = 0;

  bool EncodeRC = false;

  // Classify VEX_B, VEX_4V, VEX_R, VEX_X
  unsigned NumOps = Desc.getNumOperands();
  unsigned CurOp = X86II::getOperandBias(Desc);

  switch (TSFlags & X86II::FormMask) {
  default: llvm_unreachable("Unexpected form in EmitVEXOpcodePrefix!");
  case X86II::RawFrm:
    break;
  case X86II::MRMDestMem: {
    // MRMDestMem instructions forms:
    //  MemAddr, src1(ModR/M)
    //  MemAddr, src1(VEX_4V), src2(ModR/M)
    //  MemAddr, src1(ModR/M), imm8
    //
    unsigned BaseRegEnc = getX86RegEncoding(MI, MemOperand + X86::AddrBaseReg);
    VEX_B = ~(BaseRegEnc >> 3) & 1;
    unsigned IndexRegEnc = getX86RegEncoding(MI, MemOperand+X86::AddrIndexReg);
    VEX_X = ~(IndexRegEnc >> 3) & 1;
    if (!HasVEX_4V) // Only needed with VSIB which don't use VVVV.
      EVEX_V2 = ~(IndexRegEnc >> 4) & 1;

    CurOp += X86::AddrNumOperands;

    if (HasEVEX_K)
      EVEX_aaa = getX86RegEncoding(MI, CurOp++);

    if (HasVEX_4V) {
      unsigned VRegEnc = getX86RegEncoding(MI, CurOp++);
      VEX_4V = ~VRegEnc & 0xf;
      EVEX_V2 = ~(VRegEnc >> 4) & 1;
    }

    unsigned RegEnc = getX86RegEncoding(MI, CurOp++);
    VEX_R = ~(RegEnc >> 3) & 1;
    EVEX_R2 = ~(RegEnc >> 4) & 1;
    break;
  }
  case X86II::MRMSrcMem: {
    // MRMSrcMem instructions forms:
    //  src1(ModR/M), MemAddr
    //  src1(ModR/M), src2(VEX_4V), MemAddr
    //  src1(ModR/M), MemAddr, imm8
    //  src1(ModR/M), MemAddr, src2(Imm[7:4])
    //
    //  FMA4:
    //  dst(ModR/M.reg), src1(VEX_4V), src2(ModR/M), src3(Imm[7:4])
    unsigned RegEnc = getX86RegEncoding(MI, CurOp++);
    VEX_R = ~(RegEnc >> 3) & 1;
    EVEX_R2 = ~(RegEnc >> 4) & 1;

    if (HasEVEX_K)
      EVEX_aaa = getX86RegEncoding(MI, CurOp++);

    if (HasVEX_4V) {
      unsigned VRegEnc = getX86RegEncoding(MI, CurOp++);
      VEX_4V = ~VRegEnc & 0xf;
      EVEX_V2 = ~(VRegEnc >> 4) & 1;
    }

    unsigned BaseRegEnc = getX86RegEncoding(MI, MemOperand + X86::AddrBaseReg);
    VEX_B = ~(BaseRegEnc >> 3) & 1;
    unsigned IndexRegEnc = getX86RegEncoding(MI, MemOperand+X86::AddrIndexReg);
    VEX_X = ~(IndexRegEnc >> 3) & 1;
    if (!HasVEX_4V) // Only needed with VSIB which don't use VVVV.
      EVEX_V2 = ~(IndexRegEnc >> 4) & 1;

    break;
  }
  case X86II::MRMSrcMem4VOp3: {
    // Instruction format for 4VOp3:
    //   src1(ModR/M), MemAddr, src3(VEX_4V)
    unsigned RegEnc = getX86RegEncoding(MI, CurOp++);
    VEX_R = ~(RegEnc >> 3) & 1;

    unsigned BaseRegEnc = getX86RegEncoding(MI, MemOperand + X86::AddrBaseReg);
    VEX_B = ~(BaseRegEnc >> 3) & 1;
    unsigned IndexRegEnc = getX86RegEncoding(MI, MemOperand+X86::AddrIndexReg);
    VEX_X = ~(IndexRegEnc >> 3) & 1;

    VEX_4V = ~getX86RegEncoding(MI, CurOp + X86::AddrNumOperands) & 0xf;
    break;
  }
  case X86II::MRMSrcMemOp4: {
    //  dst(ModR/M.reg), src1(VEX_4V), src2(Imm[7:4]), src3(ModR/M),
    unsigned RegEnc = getX86RegEncoding(MI, CurOp++);
    VEX_R = ~(RegEnc >> 3) & 1;

    unsigned VRegEnc = getX86RegEncoding(MI, CurOp++);
    VEX_4V = ~VRegEnc & 0xf;

    unsigned BaseRegEnc = getX86RegEncoding(MI, MemOperand + X86::AddrBaseReg);
    VEX_B = ~(BaseRegEnc >> 3) & 1;
    unsigned IndexRegEnc = getX86RegEncoding(MI, MemOperand+X86::AddrIndexReg);
    VEX_X = ~(IndexRegEnc >> 3) & 1;
    break;
  }
  case X86II::MRM0m: case X86II::MRM1m:
  case X86II::MRM2m: case X86II::MRM3m:
  case X86II::MRM4m: case X86II::MRM5m:
  case X86II::MRM6m: case X86II::MRM7m: {
    // MRM[0-9]m instructions forms:
    //  MemAddr
    //  src1(VEX_4V), MemAddr
    if (HasVEX_4V) {
      unsigned VRegEnc = getX86RegEncoding(MI, CurOp++);
      VEX_4V = ~VRegEnc & 0xf;
      EVEX_V2 = ~(VRegEnc >> 4) & 1;
    }

    if (HasEVEX_K)
      EVEX_aaa = getX86RegEncoding(MI, CurOp++);

    unsigned BaseRegEnc = getX86RegEncoding(MI, MemOperand + X86::AddrBaseReg);
    VEX_B = ~(BaseRegEnc >> 3) & 1;
    unsigned IndexRegEnc = getX86RegEncoding(MI, MemOperand+X86::AddrIndexReg);
    VEX_X = ~(IndexRegEnc >> 3) & 1;
    break;
  }
  case X86II::MRMSrcReg: {
    // MRMSrcReg instructions forms:
    //  dst(ModR/M), src1(VEX_4V), src2(ModR/M), src3(Imm[7:4])
    //  dst(ModR/M), src1(ModR/M)
    //  dst(ModR/M), src1(ModR/M), imm8
    //
    //  FMA4:
    //  dst(ModR/M.reg), src1(VEX_4V), src2(Imm[7:4]), src3(ModR/M),
    unsigned RegEnc = getX86RegEncoding(MI, CurOp++);
    VEX_R = ~(RegEnc >> 3) & 1;
    EVEX_R2 = ~(RegEnc >> 4) & 1;

    if (HasEVEX_K)
      EVEX_aaa = getX86RegEncoding(MI, CurOp++);

    if (HasVEX_4V) {
      unsigned VRegEnc = getX86RegEncoding(MI, CurOp++);
      VEX_4V = ~VRegEnc & 0xf;
      EVEX_V2 = ~(VRegEnc >> 4) & 1;
    }

    RegEnc = getX86RegEncoding(MI, CurOp++);
    VEX_B = ~(RegEnc >> 3) & 1;
    VEX_X = ~(RegEnc >> 4) & 1;

    if (EVEX_b) {
      if (HasEVEX_RC) {
        unsigned RcOperand = NumOps-1;
        assert(RcOperand >= CurOp);
        EVEX_rc = MI.getOperand(RcOperand).getImm() & 0x3;
      }
      EncodeRC = true;
    }
    break;
  }
  case X86II::MRMSrcReg4VOp3: {
    // Instruction format for 4VOp3:
    //   src1(ModR/M), src2(ModR/M), src3(VEX_4V)
    unsigned RegEnc = getX86RegEncoding(MI, CurOp++);
    VEX_R = ~(RegEnc >> 3) & 1;

    RegEnc = getX86RegEncoding(MI, CurOp++);
    VEX_B = ~(RegEnc >> 3) & 1;

    VEX_4V = ~getX86RegEncoding(MI, CurOp++) & 0xf;
    break;
  }
  case X86II::MRMSrcRegOp4: {
    //  dst(ModR/M.reg), src1(VEX_4V), src2(Imm[7:4]), src3(ModR/M),
    unsigned RegEnc = getX86RegEncoding(MI, CurOp++);
    VEX_R = ~(RegEnc >> 3) & 1;

    unsigned VRegEnc = getX86RegEncoding(MI, CurOp++);
    VEX_4V = ~VRegEnc & 0xf;

    // Skip second register source (encoded in Imm[7:4])
    ++CurOp;

    RegEnc = getX86RegEncoding(MI, CurOp++);
    VEX_B = ~(RegEnc >> 3) & 1;
    VEX_X = ~(RegEnc >> 4) & 1;
    break;
  }
  case X86II::MRMDestReg: {
    // MRMDestReg instructions forms:
    //  dst(ModR/M), src(ModR/M)
    //  dst(ModR/M), src(ModR/M), imm8
    //  dst(ModR/M), src1(VEX_4V), src2(ModR/M)
    unsigned RegEnc = getX86RegEncoding(MI, CurOp++);
    VEX_B = ~(RegEnc >> 3) & 1;
    VEX_X = ~(RegEnc >> 4) & 1;

    if (HasEVEX_K)
      EVEX_aaa = getX86RegEncoding(MI, CurOp++);

    if (HasVEX_4V) {
      unsigned VRegEnc = getX86RegEncoding(MI, CurOp++);
      VEX_4V = ~VRegEnc & 0xf;
      EVEX_V2 = ~(VRegEnc >> 4) & 1;
    }

    RegEnc = getX86RegEncoding(MI, CurOp++);
    VEX_R = ~(RegEnc >> 3) & 1;
    EVEX_R2 = ~(RegEnc >> 4) & 1;
    if (EVEX_b)
      EncodeRC = true;
    break;
  }
  case X86II::MRM0r: case X86II::MRM1r:
  case X86II::MRM2r: case X86II::MRM3r:
  case X86II::MRM4r: case X86II::MRM5r:
  case X86II::MRM6r: case X86II::MRM7r: {
    // MRM0r-MRM7r instructions forms:
    //  dst(VEX_4V), src(ModR/M), imm8
    if (HasVEX_4V) {
      unsigned VRegEnc = getX86RegEncoding(MI, CurOp++);
      VEX_4V = ~VRegEnc & 0xf;
      EVEX_V2 = ~(VRegEnc >> 4) & 1;
    }
    if (HasEVEX_K)
      EVEX_aaa = getX86RegEncoding(MI, CurOp++);

    unsigned RegEnc = getX86RegEncoding(MI, CurOp++);
    VEX_B = ~(RegEnc >> 3) & 1;
    VEX_X = ~(RegEnc >> 4) & 1;
    break;
  }
  }

  if (Encoding == X86II::VEX || Encoding == X86II::XOP) {
    // VEX opcode prefix can have 2 or 3 bytes
    //
    //  3 bytes:
    //    +-----+ +--------------+ +-------------------+
    //    | C4h | | RXB | m-mmmm | | W | vvvv | L | pp |
    //    +-----+ +--------------+ +-------------------+
    //  2 bytes:
    //    +-----+ +-------------------+
    //    | C5h | | R | vvvv | L | pp |
    //    +-----+ +-------------------+
    //
    //  XOP uses a similar prefix:
    //    +-----+ +--------------+ +-------------------+
    //    | 8Fh | | RXB | m-mmmm | | W | vvvv | L | pp |
    //    +-----+ +--------------+ +-------------------+
    uint8_t LastByte = VEX_PP | (VEX_L << 2) | (VEX_4V << 3);

    // Can we use the 2 byte VEX prefix?
    if (Encoding == X86II::VEX && VEX_B && VEX_X && !VEX_W && (VEX_5M == 1)) {
      EmitByte(0xC5, CurByte, OS);
      EmitByte(LastByte | (VEX_R << 7), CurByte, OS);
      return;
    }

    // 3 byte VEX prefix
    EmitByte(Encoding == X86II::XOP ? 0x8F : 0xC4, CurByte, OS);
    EmitByte(VEX_R << 7 | VEX_X << 6 | VEX_B << 5 | VEX_5M, CurByte, OS);
    EmitByte(LastByte | (VEX_W << 7), CurByte, OS);
  } else {
    assert(Encoding == X86II::EVEX && "unknown encoding!");
    // EVEX opcode prefix can have 4 bytes
    //
    // +-----+ +--------------+ +-------------------+ +------------------------+
    // | 62h | | RXBR' | 00mm | | W | vvvv | U | pp | | z | L'L | b | v' | aaa |
    // +-----+ +--------------+ +-------------------+ +------------------------+
    assert((VEX_5M & 0x3) == VEX_5M
           && "More than 2 significant bits in VEX.m-mmmm fields for EVEX!");

    EmitByte(0x62, CurByte, OS);
    EmitByte((VEX_R   << 7) |
             (VEX_X   << 6) |
             (VEX_B   << 5) |
             (EVEX_R2 << 4) |
             VEX_5M, CurByte, OS);
    EmitByte((VEX_W   << 7) |
             (VEX_4V  << 3) |
             (EVEX_U  << 2) |
             VEX_PP, CurByte, OS);
    if (EncodeRC)
      EmitByte((EVEX_z  << 7) |
               (EVEX_rc << 5) |
               (EVEX_b  << 4) |
               (EVEX_V2 << 3) |
               EVEX_aaa, CurByte, OS);
    else
      EmitByte((EVEX_z  << 7) |
               (EVEX_L2 << 6) |
               (VEX_L   << 5) |
               (EVEX_b  << 4) |
               (EVEX_V2 << 3) |
               EVEX_aaa, CurByte, OS);
  }
}

/// DetermineREXPrefix - Determine if the MCInst has to be encoded with a X86-64
/// REX prefix which specifies 1) 64-bit instructions, 2) non-default operand
/// size, and 3) use of X86-64 extended registers.
uint8_t X86MCCodeEmitter::DetermineREXPrefix(const MCInst &MI, uint64_t TSFlags,
                                             int MemOperand,
                                             const MCInstrDesc &Desc) const {
  uint8_t REX = 0;
  bool UsesHighByteReg = false;

  if (TSFlags & X86II::REX_W)
    REX |= 1 << 3; // set REX.W

  if (MI.getNumOperands() == 0) return REX;

  unsigned NumOps = MI.getNumOperands();
  unsigned CurOp = X86II::getOperandBias(Desc);

  // If it accesses SPL, BPL, SIL, or DIL, then it requires a 0x40 REX prefix.
  for (unsigned i = CurOp; i != NumOps; ++i) {
    const MCOperand &MO = MI.getOperand(i);
    if (!MO.isReg()) continue;
    unsigned Reg = MO.getReg();
    if (Reg == X86::AH || Reg == X86::BH || Reg == X86::CH || Reg == X86::DH)
      UsesHighByteReg = true;
    if (X86II::isX86_64NonExtLowByteReg(Reg))
      // FIXME: The caller of DetermineREXPrefix slaps this prefix onto anything
      // that returns non-zero.
      REX |= 0x40; // REX fixed encoding prefix
  }

  switch (TSFlags & X86II::FormMask) {
  case X86II::AddRegFrm:
    REX |= isREXExtendedReg(MI, CurOp++) << 0; // REX.B
    break;
  case X86II::MRMSrcReg:
    REX |= isREXExtendedReg(MI, CurOp++) << 2; // REX.R
    REX |= isREXExtendedReg(MI, CurOp++) << 0; // REX.B
    break;
  case X86II::MRMSrcMem: {
    REX |= isREXExtendedReg(MI, CurOp++) << 2; // REX.R
    REX |= isREXExtendedReg(MI, MemOperand+X86::AddrBaseReg) << 0; // REX.B
    REX |= isREXExtendedReg(MI, MemOperand+X86::AddrIndexReg) << 1; // REX.X
    CurOp += X86::AddrNumOperands;
    break;
  }
  case X86II::MRMDestReg:
    REX |= isREXExtendedReg(MI, CurOp++) << 0; // REX.B
    REX |= isREXExtendedReg(MI, CurOp++) << 2; // REX.R
    break;
  case X86II::MRMDestMem:
    REX |= isREXExtendedReg(MI, MemOperand+X86::AddrBaseReg) << 0; // REX.B
    REX |= isREXExtendedReg(MI, MemOperand+X86::AddrIndexReg) << 1; // REX.X
    CurOp += X86::AddrNumOperands;
    REX |= isREXExtendedReg(MI, CurOp++) << 2; // REX.R
    break;
  case X86II::MRMXm:
  case X86II::MRM0m: case X86II::MRM1m:
  case X86II::MRM2m: case X86II::MRM3m:
  case X86II::MRM4m: case X86II::MRM5m:
  case X86II::MRM6m: case X86II::MRM7m:
    REX |= isREXExtendedReg(MI, MemOperand+X86::AddrBaseReg) << 0; // REX.B
    REX |= isREXExtendedReg(MI, MemOperand+X86::AddrIndexReg) << 1; // REX.X
    break;
  case X86II::MRMXr:
  case X86II::MRM0r: case X86II::MRM1r:
  case X86II::MRM2r: case X86II::MRM3r:
  case X86II::MRM4r: case X86II::MRM5r:
  case X86II::MRM6r: case X86II::MRM7r:
    REX |= isREXExtendedReg(MI, CurOp++) << 0; // REX.B
    break;
  }
  if (REX && UsesHighByteReg)
    report_fatal_error("Cannot encode high byte register in REX-prefixed instruction");

  return REX;
}

/// EmitSegmentOverridePrefix - Emit segment override opcode prefix as needed
void X86MCCodeEmitter::EmitSegmentOverridePrefix(unsigned &CurByte,
                                                 unsigned SegOperand,
                                                 const MCInst &MI,
                                                 raw_ostream &OS) const {
  // Check for explicit segment override on memory operand.
  switch (MI.getOperand(SegOperand).getReg()) {
  default: llvm_unreachable("Unknown segment register!");
  case 0: break;
  case X86::CS: EmitByte(0x2E, CurByte, OS); break;
  case X86::SS: EmitByte(0x36, CurByte, OS); break;
  case X86::DS: EmitByte(0x3E, CurByte, OS); break;
  case X86::ES: EmitByte(0x26, CurByte, OS); break;
  case X86::FS: EmitByte(0x64, CurByte, OS); break;
  case X86::GS: EmitByte(0x65, CurByte, OS); break;
  }
}

/// Emit all instruction prefixes prior to the opcode.
///
/// MemOperand is the operand # of the start of a memory operand if present.  If
/// Not present, it is -1.
///
/// Returns true if a REX prefix was used.
bool X86MCCodeEmitter::emitOpcodePrefix(uint64_t TSFlags, unsigned &CurByte,
                                        int MemOperand, const MCInst &MI,
                                        const MCInstrDesc &Desc,
                                        const MCSubtargetInfo &STI,
                                        raw_ostream &OS) const {
  bool Ret = false;
  // Emit the operand size opcode prefix as needed.
  if ((TSFlags & X86II::OpSizeMask) == (is16BitMode(STI) ? X86II::OpSize32
                                                         : X86II::OpSize16))
    EmitByte(0x66, CurByte, OS);

  // Emit the LOCK opcode prefix.
  if (TSFlags & X86II::LOCK)
    EmitByte(0xF0, CurByte, OS);

  switch (TSFlags & X86II::OpPrefixMask) {
  case X86II::PD:   // 66
    EmitByte(0x66, CurByte, OS);
    break;
  case X86II::XS:   // F3
    EmitByte(0xF3, CurByte, OS);
    break;
  case X86II::XD:   // F2
    EmitByte(0xF2, CurByte, OS);
    break;
  }

  // Handle REX prefix.
  // FIXME: Can this come before F2 etc to simplify emission?
  if (is64BitMode(STI)) {
    if (uint8_t REX = DetermineREXPrefix(MI, TSFlags, MemOperand, Desc)) {
      EmitByte(0x40 | REX, CurByte, OS);
      Ret = true;
    }
  }

  // 0x0F escape code must be emitted just before the opcode.
  switch (TSFlags & X86II::OpMapMask) {
  case X86II::TB:  // Two-byte opcode map
  case X86II::T8:  // 0F 38
  case X86II::TA:  // 0F 3A
    EmitByte(0x0F, CurByte, OS);
    break;
  }

  switch (TSFlags & X86II::OpMapMask) {
  case X86II::T8:    // 0F 38
    EmitByte(0x38, CurByte, OS);
    break;
  case X86II::TA:    // 0F 3A
    EmitByte(0x3A, CurByte, OS);
    break;
  }
  return Ret;
}



string* parsed(string line, string search)
{
//string line =  "PIM_128B_LOAD_DWORD";
 string* arr = new string[MAXSPACE];

//string arr[MAXSPACE];
//string search = "_";
int spacePos;
int currPos = 0;
int k = 0;
int prevPos = 0;

do
{

    spacePos = line.find(search,currPos);

    if(spacePos >= 0)
    {

        currPos = spacePos;
        arr[k] = line.substr(prevPos, currPos - prevPos);
        currPos++;
        prevPos = currPos;
        k++;
    }

	

}


while( spacePos >= 0);
//arr[k+1]=line.substr(prevPos, currPos - prevPos);

arr[k] = line.substr(prevPos,line.length());

/*for(int i = 0; i <= k; i++)
{
   cout << arr[i] << endl;
}
*/
return arr;


}

string HextoBinary(string sHex)
		{
			string sReturn = "";
			for (int i = 0; i < sHex.length (); ++i)
			{
				switch (sHex [i])
				{
					case '0': sReturn.append ("0000"); break;
					case '1': sReturn.append ("0001"); break;
					case '2': sReturn.append ("0010"); break;
					case '3': sReturn.append ("0011"); break;
					case '4': sReturn.append ("0100"); break;
					case '5': sReturn.append ("0101"); break;
					case '6': sReturn.append ("0110"); break;
					case '7': sReturn.append ("0111"); break;
					case '8': sReturn.append ("1000"); break;
					case '9': sReturn.append ("1001"); break;
					case 'a': 
					case 'A':
					sReturn.append ("1010"); break;
					case 'b': 
					case 'B':
					sReturn.append ("1011"); break;
					case 'c': 
					case 'C':
					sReturn.append ("1100"); break;
					case 'd': 
					case 'D':
					sReturn.append ("1101"); break;
					case 'e': 
					case 'E':
					sReturn.append ("1110"); break;
					case 'f': 
					case 'F':
					sReturn.append ("1111"); break;
				}
			}
			return sReturn;
		}


string BintoHex(string sBinary)
		{
		string rest(""),tmp,chr = "0000";
	int len = sBinary.length()/4;
	chr = chr.substr(0,len);
//	sBinary = chr+sBinary;
	for (int i=0;i<sBinary.length();i+=4)
	{
		tmp = sBinary.substr(i,4);
		if (!tmp.compare("0000"))
		{
			rest = rest + "0";
		}
		else if (!tmp.compare("0001"))
		{
			rest = rest + "1";
		}
		else if (!tmp.compare("0010"))
		{
			rest = rest + "2";
		}
		else if (!tmp.compare("0011"))
		{
			rest = rest + "3";
		}
		else if (!tmp.compare("0100"))
		{
			rest = rest + "4";
		}
		else if (!tmp.compare("0101"))
		{
			rest = rest + "5";
		}
		else if (!tmp.compare("0110"))
		{
			rest = rest + "6";
		}
		else if (!tmp.compare("0111"))
		{
			rest = rest + "7";
		}
		else if (!tmp.compare("1000"))
		{
			rest = rest + "8";
		}
		else if (!tmp.compare("1001"))
		{
			rest = rest + "9";
		}
		else if (!tmp.compare("1010"))
		{
			rest = rest + "A";
		}
		else if (!tmp.compare("1011"))
		{
			rest = rest + "B";
		}
		else if (!tmp.compare("1100"))
		{
			rest = rest + "C";
		}
		else if (!tmp.compare("1101"))
		{
			rest = rest + "D";
		}
		else if (!tmp.compare("1110"))
		{
			rest = rest + "E";
		}
		else if (!tmp.compare("1111"))
		{
			rest = rest + "F";
		}
		else
		{
			continue;
		}
	}
	return rest;
		}





string binary(int num)
{
 
 string bin="";

 
 
    int rem;


    if (num > 0) {
      int index = 1;
      while (index <= num)
         index *= 2;
      index /= 2;

      do {
        if (num >= index) {
          // cout << '1';
           bin.append("1");
           num -= index;
        }
        else
          // cout << '0';
        bin.append("0");
        index /= 2;
      } while (index > 0);
     // cout << std::endl;
    }
    else
     // cout << '0';
 
         bin.append("0");

 
 
 
 
 
 
   /* int rem;
    
 
    if (num <= 1)
    {
       // cout << num;
        bin.append(std::to_string(num));
        return bin;
    }
    rem = num % 2;
    binary(num / 2);
   // cout << rem;
    bin.append(std::to_string(rem)); */
    
    return bin;
}


// Returns '0' for '1' and '1' for '0'
char flip(char c) {return (c == '0')? '1': '0';}




std::string decimal_to_binary(int n){
     
std::string bin="";

if(n>=0)
{
bin=binary(n);
}

else if(n<0)

{
bin=binary(-n);
bin="0"+bin;
    int n = bin.length();
    int i;
 
    string ones, twos;
    ones = twos = "";
 
    //  for ones complement flip every bit
    for (i = 0; i < n; i++)
        ones += flip(bin[i]);
 
    //  for two's complement go from right to left in
    //  ones complement and if we get 1 make, we make
    //  them 0 and keep going left when we get first
    //  0, make that 1 and go out of loop
    twos = ones;
    for (i = n - 1; i >= 0; i--)
    {
        if (ones[i] == '1')
            twos[i] = '0';
        else
        {
            twos[i] = '1';
            break;
        }
    }
 
    // If No break : all are 1  as in 111  or  11111;
    // in such case, add extra 1 at beginning
    if (i == -1)
        twos = '1' + twos;
bin="";
bin= twos;

}



    return bin;
} 




std::string reverse_pairs(std::string const & src)
{
    assert(src.size() % 2 == 0);
    std::string result;
    result.reserve(src.size());

    for (std::size_t i = src.size(); i != 0; i -= 2)
    {
        result.append(src, i - 2, 2);
    }

    return result;
}


///////////////////get the binary value of registers//////////////

string Reg_Encoding(string gprr)
		{
	string	gpr_bin;	
			if(gprr=="RIP" || gprr=="EIP")
	
	{
	gpr_bin="001101";	
		
	}
	
	else if(gprr=="RAX" || gprr=="EAX")
	
	{
	gpr_bin="000000";	
		
	}
	
		else if(gprr=="RBP" || gprr=="EBP")
	
	{
	gpr_bin="000101";	
		
	}
	
	else if(gprr=="RSP" || gprr=="ESP")
	
	{
	gpr_bin="000100";	
		
	}
	
	
		else if(gprr=="RDI" || gprr=="EDI")
	
	{
	gpr_bin="000111";	
		
	}
	
	
	
		else if(gprr=="RSI" || gprr=="ESI")
	
	{
	gpr_bin="000110";	
		
	}
	
		else if(gprr=="RCX" || gprr=="ECX" )
	
	{
	gpr_bin="000001";	
		
	}
	
	
	else if(gprr=="RBX" || gprr=="EBX")
	
	{
	gpr_bin="000011";	
		
	}
	
	
	else if(gprr=="RDX" || gprr=="EDX")
	
	{
	gpr_bin="000010";	
		
	}
	
	
	else if(gprr=="R8")
	
	{
	gpr_bin="001000";	
		
	}
	
	
		else if(gprr=="R9")
	
	{
	gpr_bin="001001";	
		
	}
	
		else if(gprr=="R10")
	
	{
	gpr_bin="001010";	
		
	}
	
	
	else if(gprr=="R11")
	
	{
	gpr_bin="001011";	
		
	}
	
		else if(gprr=="R12")
	
	{
	gpr_bin="001100";	
		
	}
	
		else if(gprr=="R13") 
	
	{
	gpr_bin="011101";	///////////////not assign 13 here because 13 is assigned to RIP
		
	}
	
	else if(gprr=="R14")
	
	{
	gpr_bin="001110";	
		
	}
	
		else if(gprr=="R15")
	
	{
	gpr_bin="001111";	
		
	}



else if(gprr.substr(0,3)=="XMM"||gprr.substr(0,3)=="YMM"||gprr.substr(0,3)=="ZMM")
	
	{
	
if(gprr.substr(3,2)=="0")
	
	{
	gpr_bin="00000";	
			
	}

else if(gprr.substr(3,2)=="1")
	
	{
	gpr_bin="00001";	
			
	}

else if(gprr.substr(3,2)=="2")
	
	{
	gpr_bin="00010";
		
	}

else if(gprr.substr(3,2)=="3")
	
	{
	gpr_bin="00011";	
			
	}

else if(gprr.substr(3,2)=="4")
	
	{
	gpr_bin="00100";	
			
	}

else if(gprr.substr(3,2)=="5")
	
	{
	gpr_bin="00101";
		
	}

else if(gprr.substr(3,2)=="6")
	
	{
	gpr_bin="00110";	
			
	}

else if(gprr.substr(3,2)=="7")
	
	{
	gpr_bin="00111";	
			
	}


else if(gprr.substr(3,2)=="8")
	
	{
	gpr_bin="01000";
		
	}

else if(gprr.substr(3,2)=="9")
	
	{
	gpr_bin="01001";	
			
	}

else if(gprr.substr(3,2)=="10")
	
	{
	gpr_bin="01010";	
			
	}


else if(gprr.substr(3,2)=="11")
	
	{
	gpr_bin="01011";
		
	}

else if(gprr.substr(3,2)=="12")
	
	{
	gpr_bin="01100";	
			
	}

else if(gprr.substr(3,2)=="13")
	
	{
	gpr_bin="01101";	
			
	}
	
else if(gprr.substr(3,2)=="14")
	
	{
	gpr_bin="01110";
		
	}

else if(gprr.substr(3,2)=="15")
	
	{
	gpr_bin="01111";	
			
	}

else if(gprr.substr(3,2)=="16")
	
	{
	gpr_bin="10000";	
			
	}


else if(gprr.substr(3,2)=="17")
	
	{
	gpr_bin="10001";	
			
	}


else if(gprr.substr(3,2)=="18")
	
	{
	gpr_bin="10010";	
			
	}

else if(gprr.substr(3,2)=="19")
	
	{
	gpr_bin="10011";	
			
	}

else if(gprr.substr(3,2)=="20")
	
	{
	gpr_bin="10100";	
			
	}


else if(gprr.substr(3,2)=="21")
	
	{
	gpr_bin="10101";	
			
	}


else if(gprr.substr(3,2)=="22")
	
	{
	gpr_bin="10110";	
			
	}

else if(gprr.substr(3,2)=="23")
	
	{
	gpr_bin="10111";	
			
	}

else if(gprr.substr(3,2)=="24")
	
	{
	gpr_bin="11000";	
			
	}

else if(gprr.substr(3,2)=="25")
	
	{
	gpr_bin="11001";	
			
	}

else if(gprr.substr(3,2)=="26")
	
	{
	gpr_bin="11010";	
			
	}

else if(gprr.substr(3,2)=="27")
	
	{
	gpr_bin="11011";	
			
	}

else if(gprr.substr(3,2)=="28")
	
	{
	gpr_bin="11100";	
			
	}

else if(gprr.substr(3,2)=="29")
	
	{
	gpr_bin="11101";	
			
	}

else if(gprr.substr(3,2)=="30")
	
	{
	gpr_bin="11110";	
			
	}

else if(gprr.substr(3,2)=="31")
	
	{
	gpr_bin="11111";	
			
	}

}
////////////////////////////////////////mask registers of x86////////////////////
else if(gprr=="K0") 
	
	{
	gpr_bin="0000";	
		
	}


else if(gprr=="K1") 
	
	{
	gpr_bin="0001";	
		
	}


else if(gprr=="K2") 
	
	{
	gpr_bin="0010";	
		
	}


else if(gprr=="K3") 
	
	{
	gpr_bin="0011";	
		
	}


else if(gprr=="K4") 
	
	{
	gpr_bin="0100";	
		
	}



else if(gprr=="K5") 
	
	{
	gpr_bin="0101";	
		
	}


else if(gprr=="K6") 
	
	{
	gpr_bin="0110";	
		
	}


else if(gprr=="K7") 
	
	{
	gpr_bin="0111";	
		
	}

	
		return gpr_bin;	
			
		}






void X86MCCodeEmitter::encodePIM(const MCInst &MI, raw_ostream &OS,
                         SmallVectorImpl<MCFixup> &Fixups,
const MCSubtargetInfo &STI) const{

unsigned Opcode = MI.getOpcode();
 string* instr_arr = new string[MAXSPACE];
 string* reg_arr = new string[MAXSPACE];
  string* reg_arr_src1 = new string[MAXSPACE];

 string* reg_arr_src2 = new string[MAXSPACE];


//string instr_arr[MAXSPACE], reg_arr[MAXSPACE];

int RVU_Dest, RVU_Regno, RVU_src1,RVU_Regno_src1, RVU_src2, RVU_Regno_src2, lng, counnt, TYPE, imm_src2, imm_src3; ///////declare variables

string encoded, RVU_Dest_bin,RVU_Regno_bin,RVU_src1_bin, RVU_src2_bin, RVU_Regno_src2_bin, RVU_Regno_src1_bin,bin_fields, hex_fields, zero_str="", gpr,gpr2, gpr3, gpr_bin, gpr2_bin, gpr3_bin, imm_bin, reg_name, imm_src2_bin, imm_src3_bin;
 // std::string Opcode = MRI.getName(MI.getOperand(0).getReg());
  const MCInstrDesc &Desc = MCII.get(Opcode);

 //   unsigned RegEnc = getX86RegEncoding(MI, 0);
    
  std::string instr=MCII.getName(Opcode).str();
  
/*  while(instr.find("_"))
  {
lenggth++;}
*/

instr_arr=parsed(instr,"_");  ///////////separate the instruction fields

if(instr_arr[2]=="LOAD" || instr_arr[2]=="ZEXTSLOAD" || instr_arr[2]== "ZEXTSVLOAD")   //////////if upcoming instruction is load instruction
    
    
{



int lenggth=0, i=0;size_t contain;
reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(0).getReg()); ////////////in case of load 0th operand is destination register
gpr=Ctx.getRegisterInfo()->getName(MI.getOperand(1).getReg()); ////////////in case of load 1st operand is gpr RIP register




if(MI.getOperand(2).getImm()==1 && MI.getOperand(3).getReg()==0 && MI.getOperand(4).isExpr())
	
	
	{
	TYPE=3; /////////////TYPE 3 LOAD	
	}

else if(MI.getOperand(2).getImm()==1 && MI.getOperand(3).getReg()==0 && MI.getOperand(4).isImm())
	
	
	{
	TYPE=4; /////////////TYPE 3 LOAD	
	}



else
	
	{
		TYPE =5;  /////////////TYPE 5 LOAD
		
		
	}


if(TYPE==5 && gpr=="")

{



TYPE=4;

gpr=Ctx.getRegisterInfo()->getName(MI.getOperand(3).getReg());



}


while( 1 )   ///////////////count the occurences of _ in reg_name
{
    contain = reg_name.find("_", i);
    if (contain != string::npos )
    {
        lenggth++;
        i = contain + 1;
    }
    else
    {
        break;
    }
}




reg_arr=parsed(reg_name,"_");  ////////////parse reg_name


RVU_Dest=std::stoi(reg_arr[1]);    ////////////the 2nd field of reg_name indicates RVU number

RVU_Regno=std::stoi(reg_arr[lenggth]);



if(instr_arr[1]=="32B") ///////////for setting the opcodes accordingly

{


if (TYPE==3)	
{

if(instr_arr[2]=="LOAD")

{encoded="610043";}

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612043";}

else if(instr_arr[3]=="QWORD")
{encoded="612143";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612243";}

else if(instr_arr[3]=="QWORD")
{encoded="612343";}

}

}

else if(TYPE==4)
{	
if(instr_arr[2]=="LOAD"){
encoded="610063"; 

}

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612063";}

else if(instr_arr[3]=="QWORD")
{encoded="612163";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612263";}

else if(instr_arr[3]=="QWORD")
{encoded="612363";}

}


}


else if(TYPE==5)
{	

if(instr_arr[2]=="LOAD")

{
encoded="610083"; 
}

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612083";}

else if(instr_arr[3]=="QWORD")
{encoded="612183";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612283";}

else if(instr_arr[3]=="QWORD")
{encoded="612383";}

}

}


}

else if(instr_arr[1]=="64B") ///////////for setting the opcodes accordingly

{


if (TYPE==3)	
{

if(instr_arr[2]=="LOAD")
{

encoded="610044";
}

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612044";}

else if(instr_arr[3]=="QWORD")
{encoded="612144";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612244";}

else if(instr_arr[3]=="QWORD")
{encoded="612344";}

}



}

else if(TYPE==4)
{	

if(instr_arr[2]=="LOAD")
{

encoded="610064"; 
}


else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612064";}

else if(instr_arr[3]=="QWORD")
{encoded="612164";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612264";}

else if(instr_arr[3]=="QWORD")
{encoded="612364";}

}



}


else if(TYPE==5)
{	

if(instr_arr[2]=="LOAD")

{
encoded="610084"; 

}

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612084";}

else if(instr_arr[3]=="QWORD")
{encoded="612184";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612284";}

else if(instr_arr[3]=="QWORD")
{encoded="612384";}

}



}


}











else if(instr_arr[1]=="128B") ///////////for setting the opcodes accordingly

{


if (TYPE==3)	
{
if(instr_arr[2]=="LOAD")

{


encoded="610045";

}

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612045";}

else if(instr_arr[3]=="QWORD")
{encoded="612145";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612245";}

else if(instr_arr[3]=="QWORD")
{encoded="612345";}

}



}

else if(TYPE==4)
{	

if(instr_arr[2]=="LOAD"){

encoded="610065"; }
else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612065";}

else if(instr_arr[3]=="QWORD")
{encoded="612165";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612265";}

else if(instr_arr[3]=="QWORD")
{encoded="612365";}

}


}


else if(TYPE==5)
{	
if(instr_arr[2]=="LOAD"){
encoded="610085"; }

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612085";}

else if(instr_arr[3]=="QWORD")
{encoded="612185";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612285";}

else if(instr_arr[3]=="QWORD")
{encoded="612385";}

}
}

}



else if(instr_arr[1]=="256B")

{




if (TYPE==3){

if(instr_arr[2]=="LOAD"){
encoded="610046";
}

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612046";}

else if(instr_arr[3]=="QWORD")
{encoded="612146";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612246";}

else if(instr_arr[3]=="QWORD")
{encoded="612346";}

}

}

else if(TYPE==4)
{	
if(instr_arr[2]=="LOAD"){

encoded="610066"; }

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612066";}

else if(instr_arr[3]=="QWORD")
{encoded="612166";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612266";}

else if(instr_arr[3]=="QWORD")
{encoded="612366";}

}
}

else if (TYPE==5){

if(instr_arr[2]=="LOAD")
{
encoded="610086";}


else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612086";}

else if(instr_arr[3]=="QWORD")
{encoded="612186";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612286";}

else if(instr_arr[3]=="QWORD")
{encoded="612386";}

}
}

}



else if(instr_arr[1]=="512B")

{




if (TYPE==3){

if(instr_arr[2]=="LOAD"){



encoded="610047"; }

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612047";}

else if(instr_arr[3]=="QWORD")
{encoded="612147";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612247";}

else if(instr_arr[3]=="QWORD")
{encoded="612347";}

}
}


else if(TYPE==4)
{	
if(instr_arr[2]=="LOAD"){
encoded="610067"; }


else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612067";}

else if(instr_arr[3]=="QWORD")
{encoded="612167";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612267";}

else if(instr_arr[3]=="QWORD")
{encoded="612367";}

}

}

else if (TYPE==5){
if(instr_arr[2]=="LOAD"){

encoded="610087"; }


else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612087";}

else if(instr_arr[3]=="QWORD")
{encoded="612187";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612287";}

else if(instr_arr[3]=="QWORD")
{encoded="612387";}

}



}

}


else if(instr_arr[1]=="1024B")

{



if (TYPE==3){
if(instr_arr[2]=="LOAD"){

encoded="610048";
}

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612048";}

else if(instr_arr[3]=="QWORD")
{encoded="612148";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612248";}

else if(instr_arr[3]=="QWORD")
{encoded="612348";}

}


}

else if(TYPE==4)
{	
if(instr_arr[2]=="LOAD"){
encoded="610068"; }

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612068";}

else if(instr_arr[3]=="QWORD")
{encoded="612168";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612268";}

else if(instr_arr[3]=="QWORD")
{encoded="612368";}

}


}

else if (TYPE==5){
if(instr_arr[2]=="LOAD"){
encoded="610088"; }

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612088";}

else if(instr_arr[3]=="QWORD")
{encoded="612188";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612288";}

else if(instr_arr[3]=="QWORD")
{encoded="612388";}

}



}
}

else if(instr_arr[1]=="2048B")

{



if (TYPE==3){
if(instr_arr[2]=="LOAD"){

encoded="610049";
}

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612049";}

else if(instr_arr[3]=="QWORD")
{encoded="612149";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612249";}

else if(instr_arr[3]=="QWORD")
{encoded="612349";}

}


}

else if(TYPE==4)
{	

if(instr_arr[2]=="LOAD"){
encoded="610069"; }

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612069";}

else if(instr_arr[3]=="QWORD")
{encoded="612169";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612269";}

else if(instr_arr[3]=="QWORD")
{encoded="612369";}

}



}

else if (TYPE==5){

if(instr_arr[2]=="LOAD"){

encoded="610089"; }


else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612089";}

else if(instr_arr[3]=="QWORD")
{encoded="612189";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="612289";}

else if(instr_arr[3]=="QWORD")
{encoded="612389";}

}




}

}


else if(instr_arr[1]=="4096B")

{


	
		if (TYPE==3){
if(instr_arr[2]=="LOAD"){
encoded="61004A";
}

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="61204A";}

else if(instr_arr[3]=="QWORD")
{encoded="61214A";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="61224A";}

else if(instr_arr[3]=="QWORD")
{encoded="61234A";}

}




}

else if(TYPE==4)
{	

if(instr_arr[2]=="LOAD"){
encoded="61006A"; }

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="61206A";}

else if(instr_arr[3]=="QWORD")
{encoded="61216A";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="61226A";}

else if(instr_arr[3]=="QWORD")
{encoded="61236A";}

}




}

else if (TYPE==5){
if(instr_arr[2]=="LOAD"){
encoded="61008A"; }

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="61208A";}

else if(instr_arr[3]=="QWORD")
{encoded="61218A";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="61228A";}

else if(instr_arr[3]=="QWORD")
{encoded="61238A";}

}



}

}



else if(instr_arr[1]=="8192B")

{


		if (TYPE==3){
if(instr_arr[2]=="LOAD"){

encoded="61004B";

}

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="61204B";}

else if(instr_arr[3]=="QWORD")
{encoded="61214B";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="61224B";}

else if(instr_arr[3]=="QWORD")
{encoded="61234B";}

}










}


else if(TYPE==4)
{	
if(instr_arr[2]=="LOAD"){

encoded="61006B"; }



else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="61206B";}

else if(instr_arr[3]=="QWORD")
{encoded="61216B";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="61226B";}

else if(instr_arr[3]=="QWORD")
{encoded="61236B";}

}





}
else if (TYPE==5){
if(instr_arr[2]=="LOAD"){
encoded="61008B"; }

else if(instr_arr[2]=="ZEXTSLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="61208B";}

else if(instr_arr[3]=="QWORD")
{encoded="61218B";}

}

else if(instr_arr[2]=="ZEXTSVLOAD")

{

if(instr_arr[3]=="DWORD")
{encoded="61228B";}

else if(instr_arr[3]=="QWORD")
{encoded="61238B";}

}

}

}




RVU_Dest_bin=binary(RVU_Dest); ////////////convert the RVU no into binary
lng=RVU_Dest_bin.length();
zero_str="";

if (lng<5) ////////////append required no of 0s
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Dest_bin=zero_str+RVU_Dest_bin;
	
	
}
RVU_Regno_bin=binary(RVU_Regno);

lng=RVU_Regno_bin.length();
zero_str="";

if (lng<14)
{
	counnt=14-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Regno_bin=zero_str+RVU_Regno_bin;
	
	
}

gpr_bin=Reg_Encoding(gpr);
bin_fields="";


/////////////////


/*else if (TYPE==4 || TYPE==5)
{


imm_src2=MI.getOperand(2).getImm();
	
	
	imm_src2_bin=binary(imm_src2);

lng=imm_src2_bin.length();
zero_str="";

if (lng<32)
{
	counnt=32-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
imm_src2_bin=zero_str+imm_src2_bin;
	
	
}

}*/


/*else if (TYPE==4)	


{

bin_fields=RVU_Dest_bin+RVU_Regno_bin+gpr_bin+"0000000";



} ////////////append all the encoded fields in 1


*/
	

	
	
if (TYPE==3||TYPE==4)
{bin_fields=RVU_Dest_bin+RVU_Regno_bin+gpr_bin+"0000000"; } ////////////append all the encoded fields in 1
	
	
	

	
else if (TYPE==5)
{
	
	gpr2=Ctx.getRegisterInfo()->getName(MI.getOperand(3).getReg()); ////////////in case of load 1st operand is gpr RIP register

	gpr2_bin=Reg_Encoding(gpr2);

bin_fields=RVU_Dest_bin+RVU_Regno_bin+gpr_bin+gpr2_bin+"0"; 

} ////////////append all the encoded fields in 1


	
	

hex_fields="";
hex_fields=BintoHex(bin_fields);  ////////////convert to hex

//hex_fields.erase(0,2);  /////////////remove 0x
encoded=encoded+hex_fields;  ///////////////append opcode in encoded string


 //OS << (char*)encoded.c_str();



 int init=0, final=encoded.length();
 uint8_t ul;
 std::string str="";

 while(init!=final)

 {

	 str="0x"+encoded.substr(init,2);  
	  ul = std::stoul (str,nullptr,0);
	  OS << (char)ul;        ////////////////write the encoded string byte by byte
	 init+=2;

 }












//////////////


SMLoc Loc=MI.getLoc();
unsigned Size=4;
unsigned CurByte;

if (TYPE==3||TYPE==4||TYPE==5)

{CurByte = 7;}

/*else if (TYPE==4)

{CurByte = 7;}

*/
  
  
   int ImmOffset =0;


  if((TYPE==3 && gpr=="RIP") ||(TYPE==5 && gpr2=="RIP") ||(TYPE==4 && gpr=="RIP"))
      {
ImmOffset -= 4;}



  
bool Rex = false;

      
        unsigned FixupKind = [=]() {
      if((TYPE==3 && gpr=="RIP") ||(TYPE==5 && gpr2=="RIP")||(TYPE==4 && gpr=="RIP"))
      {
      	  
    return X86::reloc_riprel_4byte;}
    
    else  
    	
    	{
    		
    	return	X86::reloc_signed_4byte;
    	}
     
      
    }();






  const MCExpr *Expr = nullptr;

if (TYPE==3)
{Expr = MI.getOperand(4).getExpr();

     
      
    



  if (ImmOffset)
    Expr = MCBinaryExpr::createAdd(Expr, MCConstantExpr::create(ImmOffset, Ctx),
                                   Ctx);

  // Emit a symbolic constant as a fixup and 4 zeros.
  Fixups.push_back(MCFixup::create(CurByte, Expr, MCFixupKind(FixupKind), Loc));
  EmitConstant(0, Size, CurByte, OS);

}



else if (TYPE==4||TYPE==5)
{


EmitImmediate(MI.getOperand(2), MI.getLoc(), 4, MCFixupKind(FixupKind), CurByte, OS, Fixups);
CurByte = 11;

EmitImmediate(MI.getOperand(4), MI.getLoc(), 4, MCFixupKind(FixupKind), CurByte, OS, Fixups);

}





}
























else if(instr_arr[2]=="STORE")
    
    
{ int lenggth=0, i=0;size_t contain;
reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(5).getReg());  ///in case of store 5th operand is source register
gpr=Ctx.getRegisterInfo()->getName(MI.getOperand(0).getReg()); //1st operand is gpr RIP
//int indxr=reg_name.length()-1;

reg_arr=parsed(reg_name,"_");

if(MI.getOperand(1).getImm()==1 && MI.getOperand(2).getReg()==0 && MI.getOperand(3).isExpr())
	
	
	{
	TYPE=3; /////////////TYPE 3 store	
	}


else if(MI.getOperand(1).getImm()==1 && MI.getOperand(2).getReg()==0 && MI.getOperand(3).isImm())
	
	
	{
	TYPE=4; /////////////TYPE 4 store	
	}









else
	
	{
		TYPE =5;  /////////////TYPE 5 store
		
		
	}




if(TYPE==5 && gpr=="")

{



TYPE=4;

gpr=Ctx.getRegisterInfo()->getName(MI.getOperand(2).getReg());



}






while( 1 )
{
    contain = reg_name.find("_", i);
    if (contain != string::npos )
    {
        lenggth++;
        i = contain + 1;
    }
    else
    {
        break;
    }
}




RVU_Dest=std::stoi(reg_arr[1]); //////////compute RVU no which is 1st field after _ in RVU register

RVU_Regno=std::stoi(reg_arr[lenggth]);





if(instr_arr[1]=="32B") ///////////for setting the opcodes accordingly

{


if (TYPE==3)	
{encoded="610143";}

else if(TYPE==4)
{	encoded="610163"; }

else if(TYPE==5)
{	encoded="610183"; }

}


else if(instr_arr[1]=="64B") ///////////for setting the opcodes accordingly

{


if (TYPE==3)	
{encoded="610144";}

else if(TYPE==4)
{	encoded="610164"; }

else if(TYPE==5)
{	encoded="610184"; }

}



else if(instr_arr[1]=="128B") ///////////for setting the opcodes accordingly

{


if (TYPE==3)	
{encoded="610145";}

else if(TYPE==4)
{	encoded="610165"; }

else if(TYPE==5)
{	encoded="610185"; }

}




else if(instr_arr[1]=="256B")

{



if (TYPE==3){
encoded="610146";}

else if(TYPE==4)
{	encoded="610166"; }

else if (TYPE==5){
encoded="610186";}

}


else if(instr_arr[1]=="512B")

{



if (TYPE==3){

encoded="610147"; }


else if(TYPE==4)
{	encoded="610167"; }

else if (TYPE==5){

encoded="610187"; }


}


else if(instr_arr[1]=="1024B")

{


if (TYPE==3){


encoded="610148";
}

else if(TYPE==4)
{	encoded="610168"; }

else if (TYPE==5){

encoded="610188"; }



}


else if(instr_arr[1]=="2048B")

{



if (TYPE==3){

encoded="610149";
}

else if(TYPE==4)
{	encoded="610169"; }

else if (TYPE==5){

encoded="610189"; }

}



else if(instr_arr[1]=="4096B")

{


	
		if (TYPE==3){

encoded="61014A";
}

else if(TYPE==4)
{	encoded="61016A"; }

else if (TYPE==5){

encoded="61018A"; }


}



else if(instr_arr[1]=="8192B")

{


		if (TYPE==3){


encoded="61014B";

}

else if(TYPE==4)
{	encoded="61016B"; }


else if (TYPE==5){

encoded="61018B"; }


}




RVU_Dest_bin=binary(RVU_Dest);
lng=RVU_Dest_bin.length();
zero_str="";

if (lng<5)
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Dest_bin=zero_str+RVU_Dest_bin;
	
	
}
RVU_Regno_bin=binary(RVU_Regno);

lng=RVU_Regno_bin.length();
zero_str="";

if (lng<14)
{
	counnt=14-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Regno_bin=zero_str+RVU_Regno_bin;
	
	
}

gpr_bin=Reg_Encoding(gpr);

	bin_fields="";

	////////////////////



if (TYPE==3||TYPE==4)
	
{bin_fields=RVU_Dest_bin+RVU_Regno_bin+gpr_bin+"0000000";} ////////////append all the encoded fields in 1





else if (TYPE==5)
{
	
	gpr2=Ctx.getRegisterInfo()->getName(MI.getOperand(2).getReg()); ////////////in case of load 1st operand is gpr RIP register

	gpr2_bin=Reg_Encoding(gpr2);


	
bin_fields=RVU_Dest_bin+RVU_Regno_bin+gpr_bin+gpr2_bin+"0"; 

} ////////////append all the encoded fields in 1











	

hex_fields="";
hex_fields=BintoHex(bin_fields);

encoded=encoded+hex_fields;

 int init=0, final=encoded.length();
 uint8_t ul;
 std::string str="";

 while(init!=final)

 {

	 str="0x"+encoded.substr(init,2);
	  ul = std::stoul (str,nullptr,0);
	  OS << (char)ul;
	 init+=2;

 }





const MCExpr *Expr = nullptr;


SMLoc Loc=MI.getLoc();
unsigned Size=4;
  
  
  unsigned CurByte;

if (TYPE==3||TYPE==4||TYPE==5)

{CurByte = 7;}


  
  
  
  
  
  int ImmOffset =0;
   
	
	 if((TYPE==3 && gpr=="RIP") ||(TYPE==5 && gpr2=="RIP")||(TYPE==4 && gpr=="RIP"))
      {
ImmOffset -= 4;}

// ImmOffset -= 4;
//CurByte=7;
bool Rex = false;

unsigned FixupKind = [=]() {
       if((TYPE==3 && gpr=="RIP") ||(TYPE==5 && gpr2=="RIP")||(TYPE==4 && gpr=="RIP"))
      {
      	  
    return X86::reloc_riprel_4byte;}
    
    else  
    	
    	{
    		
    	return	X86::reloc_signed_4byte;
    	}
     
      
    }();

if (TYPE==3)
	
	{
 Expr = MI.getOperand(3).getExpr();

  if (ImmOffset)
    Expr = MCBinaryExpr::createAdd(Expr, MCConstantExpr::create(ImmOffset, Ctx),
                                   Ctx);

  // Emit a symbolic constant as a fixup and 4 zeros.
  Fixups.push_back(MCFixup::create(CurByte, Expr, MCFixupKind(FixupKind), Loc));
  EmitConstant(0, Size, CurByte, OS);


}


else if (TYPE==4||TYPE==5)
{

EmitImmediate(MI.getOperand(1), MI.getLoc(), 4, MCFixupKind(FixupKind), CurByte, OS, Fixups);

CurByte = 11;

EmitImmediate(MI.getOperand(3), MI.getLoc(), 4, MCFixupKind(FixupKind), CurByte, OS, Fixups);

}


}



else if(instr_arr[2]=="BROADCASTS"||instr_arr[2]=="BROADCASTD")
	
	
	
{int lenggth=0, i=0;size_t contain;
 reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(0).getReg()); ////////////in case of load 0th operand is destination register
 gpr=Ctx.getRegisterInfo()->getName(MI.getOperand(1).getReg()); ////////////in case of load 1st operand is gpr RIP register 
//int indxr=reg_name.length()-1;

TYPE=3;



if (MI.getOperand(3).getReg()!=0)
{

gpr2=Ctx.getRegisterInfo()->getName(MI.getOperand(3).getReg());
TYPE=5;


}





while( 1 )   ///////////////count the occurences of _ in reg_name
{
    contain = reg_name.find("_", i);
    if (contain != string::npos )
    {
        lenggth++;
        i = contain + 1;
    }
    else
    {
        break;
    }
}




reg_arr=parsed(reg_name,"_");  ////////////parse reg_name


RVU_Dest=std::stoi(reg_arr[1]);    ////////////the 2nd field of reg_name indicates RVU number

RVU_Regno=std::stoi(reg_arr[lenggth]);


if (instr_arr[2]=="BROADCASTS")
{
if(instr_arr[1]=="128B") ///////////for setting the opcodes accordingly


{

if(TYPE==3)

{encoded="611045";}

else if(TYPE==5)

{encoded="613045";}



}





else if(instr_arr[1]=="256B")


{

if(TYPE==3)
{

encoded="611046";}


else if(TYPE==5)
{

encoded="613046";}





}




else if(instr_arr[1]=="512B")


{


if(TYPE==3)
{
encoded="611047";}

else if(TYPE==5)
{
encoded="613047";}




}



else if(instr_arr[1]=="1024B")

{

if(TYPE==3)
{
encoded="611048";
}

if(TYPE==5)
{
encoded="613048";
}





}



else if(instr_arr[1]=="2048B")


{

if(TYPE==3)
{
encoded="611049";
}

else if(TYPE==5)
{
encoded="613049";
}




}



else if(instr_arr[1]=="4096B")


{

if(TYPE==3)
{
encoded="61104A";
}

else if(TYPE==5)
{
encoded="61304A";
}




}




else if(instr_arr[1]=="8192B")


{
if(TYPE==3)
{
encoded="61104B";}

else if(TYPE==5)
{
encoded="61304B";}


}

}


else if (instr_arr[2]=="BROADCASTD")
{
if(instr_arr[1]=="128B") ///////////for setting the opcodes accordingly


{

if(TYPE==3)
{
encoded="611065";}

else if(TYPE==5)
{
encoded="613065";}



}





else if(instr_arr[1]=="256B")


{

if(TYPE==3)
{
encoded="611066";}


else if(TYPE==5)
{
encoded="613066";}


}




else if(instr_arr[1]=="512B")


{

if(TYPE==3)
{

encoded="611067";

}

else if(TYPE==5)
{

encoded="613067";

}

}



else if(instr_arr[1]=="1024B")



{
if(TYPE==3)
{
encoded="611068";
}

else if(TYPE==5)
{
encoded="613068";
}


}



else if(instr_arr[1]=="2048B")


{
if(TYPE==3)
{
encoded="611069";
}

else if(TYPE==5)
{
encoded="613069";
}

}



else if(instr_arr[1]=="4096B")


{

if(TYPE==3)
{
encoded="61106A";
}

else if(TYPE==5)
{
encoded="61306A";
}



}




else if(instr_arr[1]=="8192B")


{

if(TYPE==3)
{
encoded="61106B";
}

else if(TYPE==5)
{
encoded="61306B";
}


}

}


RVU_Dest_bin=binary(RVU_Dest); ////////////convert the RVU no into binary
lng=RVU_Dest_bin.length();
zero_str="";

if (lng<5) ////////////append required no of 0s
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Dest_bin=zero_str+RVU_Dest_bin;
	
	
}
RVU_Regno_bin=binary(RVU_Regno);

lng=RVU_Regno_bin.length();
zero_str="";

if (lng<14)
{
	counnt=14-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Regno_bin=zero_str+RVU_Regno_bin;
	
	
}
gpr_bin=Reg_Encoding(gpr);

gpr2_bin=Reg_Encoding(gpr2);

bin_fields="";


/////////////////

if(TYPE==3)

{

bin_fields=RVU_Dest_bin+RVU_Regno_bin+gpr_bin+"0000000";} ////////////append all the encoded fields in 1


else if(TYPE==5)

{

bin_fields=RVU_Dest_bin+RVU_Regno_bin+gpr_bin+gpr2_bin+"0";}  ////////////append all the encoded fields in 1




hex_fields="";
hex_fields=BintoHex(bin_fields);  ////////////convert to hex

encoded=encoded+hex_fields;  ///////////////append opcode in encoded string





 int init=0, final=encoded.length();
 uint8_t ul;
 std::string str="";

 while(init!=final)

 {

	 str="0x"+encoded.substr(init,2);  
	  ul = std::stoul (str,nullptr,0);
	  OS << (char)ul;        ////////////////write the encoded string byte by byte
	 init+=2;

 }












//////////////


SMLoc Loc=MI.getLoc();
unsigned Size=4;
  unsigned CurByte = 7;
  
  
   int ImmOffset =0;

 ImmOffset -= 4;

  
bool Rex = false;

      
        unsigned FixupKind = [=]() {
      if(gpr=="RIP")
      {
    return X86::reloc_riprel_4byte;}
    
    else  
    	
    	{
    		
    	return	X86::reloc_signed_4byte;
    	}
     
      
    }();






  const MCExpr *Expr = nullptr;



if (MI.getOperand(4).isExpr())
{Expr = MI.getOperand(4).getExpr();

     
      
    



  if (ImmOffset)
    Expr = MCBinaryExpr::createAdd(Expr, MCConstantExpr::create(ImmOffset, Ctx),
                                   Ctx);

  // Emit a symbolic constant as a fixup and 4 zeros.
  Fixups.push_back(MCFixup::create(CurByte, Expr, MCFixupKind(FixupKind), Loc));
  EmitConstant(0, Size, CurByte, OS);

}






else if (MI.getOperand(4).isImm() || MI.getOperand(2).isImm())
{


if (TYPE==5)

{


EmitImmediate(MI.getOperand(2), MI.getLoc(), 4, MCFixupKind(FixupKind), CurByte, OS,
                  Fixups);

}



EmitImmediate(MI.getOperand(4), MI.getLoc(), 4, MCFixupKind(FixupKind), CurByte, OS,
                  Fixups);

}





}


/////////////////////////////




else if(instr_arr[2]=="BROADCASTRS"||instr_arr[2]=="BROADCASTRD")
	
	
	
{int lenggth=0, i=0;size_t contain;
std::string reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(0).getReg()); ////////////in case of load 0th operand is destination register
std::string gpr=Ctx.getRegisterInfo()->getName(MI.getOperand(1).getReg()); ////////////in case of load 1st operand is gpr RIP register
//int indxr=reg_name.length()-1;




while( 1 )   ///////////////count the occurences of _ in reg_name
{
    contain = reg_name.find("_", i);
    if (contain != string::npos )
    {
        lenggth++;
        i = contain + 1;
    }
    else
    {
        break;
    }
}




reg_arr=parsed(reg_name,"_");  ////////////parse reg_name


RVU_Dest=std::stoi(reg_arr[1]);    ////////////the 2nd field of reg_name indicates RVU number

RVU_Regno=std::stoi(reg_arr[lenggth]);


if (instr_arr[2]=="BROADCASTRS")
{
if(instr_arr[1]=="128B") ///////////for setting the opcodes accordingly


{encoded="611145";}





else if(instr_arr[1]=="256B")


{encoded="611146";}




else if(instr_arr[1]=="512B")


{encoded="611147";}



else if(instr_arr[1]=="1024B")



{encoded="611148";}



else if(instr_arr[1]=="2048B")


{encoded="611149";}



else if(instr_arr[1]=="4096B")


{encoded="61114A";}




else if(instr_arr[1]=="8192B")


{encoded="61114B";}

}


else if (instr_arr[2]=="BROADCASTRD")
{
if(instr_arr[1]=="128B") ///////////for setting the opcodes accordingly


{encoded="611165";}





else if(instr_arr[1]=="256B")


{encoded="611166";}




else if(instr_arr[1]=="512B")


{encoded="611167";}



else if(instr_arr[1]=="1024B")



{encoded="611168";}



else if(instr_arr[1]=="2048B")


{encoded="611169";}



else if(instr_arr[1]=="4096B")


{encoded="61116A";}




else if(instr_arr[1]=="8192B")


{encoded="61116B";}

}














RVU_Dest_bin=binary(RVU_Dest); ////////////convert the RVU no into binary
lng=RVU_Dest_bin.length();
zero_str="";

if (lng<5) ////////////append required no of 0s
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Dest_bin=zero_str+RVU_Dest_bin;
	
	
}
RVU_Regno_bin=binary(RVU_Regno);

lng=RVU_Regno_bin.length();
zero_str="";

if (lng<14)
{
	counnt=14-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Regno_bin=zero_str+RVU_Regno_bin;
	
	
}


gpr_bin=Reg_Encoding(gpr);

bin_fields="";


/////////////////

bin_fields=RVU_Dest_bin+RVU_Regno_bin+gpr_bin+"0000000";  ////////////append all the encoded fields in 1

hex_fields="";
hex_fields=BintoHex(bin_fields);  ////////////convert to hex

encoded=encoded+hex_fields;  ///////////////append opcode in encoded string





 int init=0, final=encoded.length();
 uint8_t ul;
 std::string str="";

 while(init!=final)

 {

	 str="0x"+encoded.substr(init,2);  
	  ul = std::stoul (str,nullptr,0);
	  OS << (char)ul;        ////////////////write the encoded string byte by byte
	 init+=2;

 }

}



////////////Arithmetic Instructions/////////////////

else if(instr_arr[2]=="VADD"||instr_arr[2]=="FVADD"||instr_arr[2]=="VSUB"||instr_arr[2]=="FVSUB"||instr_arr[2]=="VMUL"
	
	||instr_arr[2]=="VMUL"||instr_arr[2]=="FVMUL"||instr_arr[2]=="VDIV"||instr_arr[2]=="FVDIV" ||instr_arr[2]=="VXOR" ||instr_arr[2]=="SHIFTL" ||instr_arr[2]=="VPERM")
    
    
{

int lenggth1=0, i=0;size_t contain;
int lenggth2=0;
int lenggth3=0;
std::string dest_reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(0).getReg()); /////////incase of add 0th operand is destination reg
std::string src1_reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(1).getReg()); ///////////1st operand is source1 reg
std::string src2_reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(2).getReg());  /////////////////2nd operand is source2 reg



while( 1 )
{
    contain = dest_reg_name.find("_", i);
    if (contain != string::npos )
    {
        lenggth1++;
        i = contain + 1;
    }
    else
    {
        break;
    }
}

i=0;
while( 1 )
{
    contain = src1_reg_name.find("_", i);
    if (contain != string::npos )
    {
        lenggth2++;
        i = contain + 1;
    }
    else
    {
        break;
    }
}

i=0;
while( 1 )
{
    contain = src2_reg_name.find("_", i);
    if (contain != string::npos )
    {
        lenggth3++;
        i = contain + 1;
    }
    else
    {
        break;
    }
}











reg_arr=parsed(dest_reg_name,"_");


RVU_Dest=std::stoi(reg_arr[1]);

RVU_Regno=std::stoi(reg_arr[lenggth1]);




reg_arr_src1=parsed(src1_reg_name,"_");


RVU_src1=std::stoi(reg_arr_src1[1]);

RVU_Regno_src1=std::stoi(reg_arr_src1[lenggth2]);



reg_arr_src2=parsed(src2_reg_name,"_");


RVU_src2=std::stoi(reg_arr_src2[1]);

RVU_Regno_src2=std::stoi(reg_arr_src2[lenggth3]);




//string imm=MI.getOperand(4).getExpr();

/*string imm;
llvm::raw_string_ostream rso(imm);
   std::unique_ptr<const llvm::MCAsmInfo> MAI;
   
MI.getOperand(4).getExpr()->print(rso, MAI);
 
*/

if(instr_arr[2]=="VADD")
	
	{

if(instr_arr[1]=="128B")

{

if(instr_arr[3]=="DWORD")
{encoded="610245";}

else if(instr_arr[3]=="QWORD")
{encoded="610265";}

}



else if(instr_arr[1]=="256B")

{

if(instr_arr[3]=="DWORD")
{encoded="610246";}

else if(instr_arr[3]=="QWORD")
{encoded="610266";}

}


else if(instr_arr[1]=="512B")

{

if(instr_arr[3]=="DWORD")
{encoded="610247";}

else if(instr_arr[3]=="QWORD")
{encoded="610267";}

}

else if(instr_arr[1]=="1024B")

{

if(instr_arr[3]=="DWORD")
{encoded="610248";}

else if(instr_arr[3]=="QWORD")
{encoded="610268";}

}

else if(instr_arr[1]=="2048B")

{

if(instr_arr[3]=="DWORD")
{encoded="610249";}

else if(instr_arr[3]=="QWORD")
{encoded="610269";}

}

else if(instr_arr[1]=="4096B")

{

if(instr_arr[3]=="DWORD")
{encoded="61024A";}

else if(instr_arr[3]=="QWORD")
{encoded="61026A";}

}


else if(instr_arr[1]=="8192B")

{

if(instr_arr[3]=="DWORD")
{encoded="61024B";}

else if(instr_arr[3]=="QWORD")
{encoded="61026B";}

}


}



else if(instr_arr[2]=="FVADD")
	
	{

if(instr_arr[1]=="128B")

{

if(instr_arr[3]=="DWORD")
{encoded="610345";}

else if(instr_arr[3]=="QWORD")
{encoded="610365";}

}

else if(instr_arr[1]=="256B")

{

if(instr_arr[3]=="DWORD")
{encoded="610346";}

else if(instr_arr[3]=="QWORD")
{encoded="610366";}

}


else if(instr_arr[1]=="512B")

{

if(instr_arr[3]=="DWORD")
{encoded="610347";}

else if(instr_arr[3]=="QWORD")
{encoded="610367";}

}

else if(instr_arr[1]=="1024B")

{

if(instr_arr[3]=="DWORD")
{encoded="610348";}

else if(instr_arr[3]=="QWORD")
{encoded="610368";}

}

else if(instr_arr[1]=="2048B")

{

if(instr_arr[3]=="DWORD")
{encoded="610349";}

else if(instr_arr[3]=="QWORD")
{encoded="610369";}

}

else if(instr_arr[1]=="4096B")

{

if(instr_arr[3]=="DWORD")
{encoded="61034A";}

else if(instr_arr[3]=="QWORD")
{encoded="61036A";}

}


else if(instr_arr[1]=="8192B")

{

if(instr_arr[3]=="DWORD")
{encoded="61034B";}

else if(instr_arr[3]=="QWORD")
{encoded="61036B";}

}


}



else if(instr_arr[2]=="VSUB")
	
	{

if(instr_arr[1]=="128B")

{

if(instr_arr[3]=="DWORD")
{encoded="610445";}

else if(instr_arr[3]=="QWORD")
{encoded="610465";}

}

else if(instr_arr[1]=="256B")

{

if(instr_arr[3]=="DWORD")
{encoded="610446";}

else if(instr_arr[3]=="QWORD")
{encoded="610466";}

}


else if(instr_arr[1]=="512B")

{

if(instr_arr[3]=="DWORD")
{encoded="610447";}

else if(instr_arr[3]=="QWORD")
{encoded="610467";}

}

else if(instr_arr[1]=="1024B")

{

if(instr_arr[3]=="DWORD")
{encoded="610448";}

else if(instr_arr[3]=="QWORD")
{encoded="610468";}

}

else if(instr_arr[1]=="2048B")

{

if(instr_arr[3]=="DWORD")
{encoded="610449";}

else if(instr_arr[3]=="QWORD")
{encoded="610469";}

}

else if(instr_arr[1]=="4096B")

{

if(instr_arr[3]=="DWORD")
{encoded="61044A";}

else if(instr_arr[3]=="QWORD")
{encoded="61046A";}

}


else if(instr_arr[1]=="8192B")

{

if(instr_arr[3]=="DWORD")
{encoded="61044B";}

else if(instr_arr[3]=="QWORD")
{encoded="61046B";}

}


}





else if(instr_arr[2]=="FVSUB")
	
	{

if(instr_arr[1]=="128B")

{

if(instr_arr[3]=="DWORD")
{encoded="610545";}

else if(instr_arr[3]=="QWORD")
{encoded="610565";}

}

else if(instr_arr[1]=="256B")

{

if(instr_arr[3]=="DWORD")
{encoded="610546";}

else if(instr_arr[3]=="QWORD")
{encoded="610566";}

}


else if(instr_arr[1]=="512B")

{

if(instr_arr[3]=="DWORD")
{encoded="610547";}

else if(instr_arr[3]=="QWORD")
{encoded="610567";}

}

else if(instr_arr[1]=="1024B")

{

if(instr_arr[3]=="DWORD")
{encoded="610548";}

else if(instr_arr[3]=="QWORD")
{encoded="610568";}

}

else if(instr_arr[1]=="2048B")

{

if(instr_arr[3]=="DWORD")
{encoded="610549";}

else if(instr_arr[3]=="QWORD")
{encoded="610569";}

}

else if(instr_arr[1]=="4096B")

{

if(instr_arr[3]=="DWORD")
{encoded="61054A";}

else if(instr_arr[3]=="QWORD")
{encoded="61056A";}

}


else if(instr_arr[1]=="8192B")

{

if(instr_arr[3]=="DWORD")
{encoded="61054B";}

else if(instr_arr[3]=="QWORD")
{encoded="61056B";}

}


}



else if(instr_arr[2]=="VXOR")
	
	{

if(instr_arr[1]=="128B")

{

if(instr_arr[3]=="DWORD")
{encoded="611745";}

else if(instr_arr[3]=="QWORD")
{encoded="611765";}

}

else if(instr_arr[1]=="256B")

{

if(instr_arr[3]=="DWORD")
{encoded="611746";}

else if(instr_arr[3]=="QWORD")
{encoded="611766";}

}


else if(instr_arr[1]=="512B")

{

if(instr_arr[3]=="DWORD")
{encoded="611747";}

else if(instr_arr[3]=="QWORD")
{encoded="611767";}

}

else if(instr_arr[1]=="1024B")

{

if(instr_arr[3]=="DWORD")
{encoded="611748";}

else if(instr_arr[3]=="QWORD")
{encoded="611768";}

}

else if(instr_arr[1]=="2048B")

{

if(instr_arr[3]=="DWORD")
{encoded="611749";}

else if(instr_arr[3]=="QWORD")
{encoded="611769";}

}

else if(instr_arr[1]=="4096B")

{

if(instr_arr[3]=="DWORD")
{encoded="61174A";}

else if(instr_arr[3]=="QWORD")
{encoded="61176A";}

}


else if(instr_arr[1]=="8192B")

{

if(instr_arr[3]=="DWORD")
{encoded="61174B";}

else if(instr_arr[3]=="QWORD")
{encoded="61176B";}

}


}










else if(instr_arr[2]=="VMUL")
	
	{

if(instr_arr[1]=="128B")

{

if(instr_arr[3]=="DWORD")
{encoded="610c45";}

else if(instr_arr[3]=="QWORD")
{encoded="610c65";}

}

else if(instr_arr[1]=="256B")

{

if(instr_arr[3]=="DWORD")
{encoded="610c46";}

else if(instr_arr[3]=="QWORD")
{encoded="610c66";}

}


else if(instr_arr[1]=="512B")

{

if(instr_arr[3]=="DWORD")
{encoded="610c47";}

else if(instr_arr[3]=="QWORD")
{encoded="610c67";}

}

else if(instr_arr[1]=="1024B")

{

if(instr_arr[3]=="DWORD")
{encoded="610c48";}

else if(instr_arr[3]=="QWORD")
{encoded="610c68";}

}

else if(instr_arr[1]=="2048B")

{

if(instr_arr[3]=="DWORD")
{encoded="610c49";}

else if(instr_arr[3]=="QWORD")
{encoded="610c69";}

}

else if(instr_arr[1]=="4096B")

{

if(instr_arr[3]=="DWORD")
{encoded="610c4A";}

else if(instr_arr[3]=="QWORD")
{encoded="610c6A";}

}


else if(instr_arr[1]=="8192B")

{

if(instr_arr[3]=="DWORD")
{encoded="610c4B";}

else if(instr_arr[3]=="QWORD")
{encoded="610c6B";}

}


}




else if(instr_arr[2]=="FVMUL")
	
	{

if(instr_arr[1]=="128B")

{

if(instr_arr[3]=="DWORD")
{encoded="610d45";}

else if(instr_arr[3]=="QWORD")
{encoded="610d65";}

}

else if(instr_arr[1]=="256B")

{

if(instr_arr[3]=="DWORD")
{encoded="610d46";}

else if(instr_arr[3]=="QWORD")
{encoded="610d66";}

}


else if(instr_arr[1]=="512B")

{

if(instr_arr[3]=="DWORD")
{encoded="610d47";}

else if(instr_arr[3]=="QWORD")
{encoded="610d67";}

}

else if(instr_arr[1]=="1024B")

{

if(instr_arr[3]=="DWORD")
{encoded="610d48";}

else if(instr_arr[3]=="QWORD")
{encoded="610d68";}

}

else if(instr_arr[1]=="2048B")

{

if(instr_arr[3]=="DWORD")
{encoded="610d49";}

else if(instr_arr[3]=="QWORD")
{encoded="610d69";}

}

else if(instr_arr[1]=="4096B")

{

if(instr_arr[3]=="DWORD")
{encoded="610d4A";}

else if(instr_arr[3]=="QWORD")
{encoded="610d6A";}

}


else if(instr_arr[1]=="8192B")

{

if(instr_arr[3]=="DWORD")
{encoded="610d4B";}

else if(instr_arr[3]=="QWORD")
{encoded="610d6B";}

}


}




else if(instr_arr[2]=="VDIV")
	
	{

if(instr_arr[1]=="128B")

{

if(instr_arr[3]=="DWORD")
{encoded="610e45";}

else if(instr_arr[3]=="QWORD")
{encoded="610e65";}

}

else if(instr_arr[1]=="256B")

{

if(instr_arr[3]=="DWORD")
{encoded="610e46";}

else if(instr_arr[3]=="QWORD")
{encoded="610e66";}

}


else if(instr_arr[1]=="512B")

{

if(instr_arr[3]=="DWORD")
{encoded="610e47";}

else if(instr_arr[3]=="QWORD")
{encoded="610e67";}

}

else if(instr_arr[1]=="1024B")

{

if(instr_arr[3]=="DWORD")
{encoded="610e48";}

else if(instr_arr[3]=="QWORD")
{encoded="610e68";}

}

else if(instr_arr[1]=="2048B")

{

if(instr_arr[3]=="DWORD")
{encoded="610e49";}

else if(instr_arr[3]=="QWORD")
{encoded="610e69";}

}

else if(instr_arr[1]=="4096B")

{

if(instr_arr[3]=="DWORD")
{encoded="610e4A";}

else if(instr_arr[3]=="QWORD")
{encoded="610e6A";}

}


else if(instr_arr[1]=="8192B")

{

if(instr_arr[3]=="DWORD")
{encoded="610e4B";}

else if(instr_arr[3]=="QWORD")
{encoded="610e6B";}

}


}



else if(instr_arr[2]=="FVDIV")
	
	{

if(instr_arr[1]=="128B")

{

if(instr_arr[3]=="DWORD")
{encoded="610f45";}

else if(instr_arr[3]=="QWORD")
{encoded="610f65";}

}

else if(instr_arr[1]=="256B")

{

if(instr_arr[3]=="DWORD")
{encoded="610f46";}

else if(instr_arr[3]=="QWORD")
{encoded="610f66";}

}


else if(instr_arr[1]=="512B")

{

if(instr_arr[3]=="DWORD")
{encoded="610f47";}

else if(instr_arr[3]=="QWORD")
{encoded="610f67";}

}

else if(instr_arr[1]=="1024B")

{

if(instr_arr[3]=="DWORD")
{encoded="610f48";}

else if(instr_arr[3]=="QWORD")
{encoded="610f68";}

}

else if(instr_arr[1]=="2048B")

{

if(instr_arr[3]=="DWORD")
{encoded="610f49";}

else if(instr_arr[3]=="QWORD")
{encoded="610f69";}

}

else if(instr_arr[1]=="4096B")

{

if(instr_arr[3]=="DWORD")
{encoded="610f4A";}

else if(instr_arr[3]=="QWORD")
{encoded="610f6A";}

}


else if(instr_arr[1]=="8192B")

{

if(instr_arr[3]=="DWORD")
{encoded="610f4B";}

else if(instr_arr[3]=="QWORD")
{encoded="610f6B";}

}
}
//////////////////End of FVDIV////////////////////

////////////////////// begin of SHIFTL//////////////

else if(instr_arr[2]=="SHIFTL")
	
	{

if(instr_arr[1]=="128B")

{

if(instr_arr[3]=="DWORD")
{encoded="610945";}

else if(instr_arr[3]=="QWORD")
{encoded="610965";}

}

else if(instr_arr[1]=="256B")

{

if(instr_arr[3]=="DWORD")
{encoded="610946";}

else if(instr_arr[3]=="QWORD")
{encoded="610966";}

}


else if(instr_arr[1]=="512B")

{

if(instr_arr[3]=="DWORD")
{encoded="610947";}

else if(instr_arr[3]=="QWORD")
{encoded="610967";}

}

else if(instr_arr[1]=="1024B")

{

if(instr_arr[3]=="DWORD")
{encoded="610948";}

else if(instr_arr[3]=="QWORD")
{encoded="610968";}

}

else if(instr_arr[1]=="2048B")

{

if(instr_arr[3]=="DWORD")
{encoded="610949";}

else if(instr_arr[3]=="QWORD")
{encoded="610969";}

}

else if(instr_arr[1]=="4096B")

{

if(instr_arr[3]=="DWORD")
{encoded="61094A";}

else if(instr_arr[3]=="QWORD")
{encoded="61096A";}

}


else if(instr_arr[1]=="8192B")

{

if(instr_arr[3]=="DWORD")
{encoded="61094B";}

else if(instr_arr[3]=="QWORD")
{encoded="61096B";}

}


}

///////////////End of SHIFTL//////////////////





///////////////begin of VPERM//////////////////



else if(instr_arr[2]=="VPERM")
	
	{

if(instr_arr[1]=="128B")

{

if(instr_arr[3]=="DWORD")
{encoded="611245";}

else if(instr_arr[3]=="QWORD")
{encoded="611265";}

}

else if(instr_arr[1]=="256B")

{

if(instr_arr[3]=="DWORD")
{encoded="611246";}

else if(instr_arr[3]=="QWORD")
{encoded="611266";}

}


else if(instr_arr[1]=="512B")

{

if(instr_arr[3]=="DWORD")
{encoded="611247";}

else if(instr_arr[3]=="QWORD")
{encoded="611267";}

}

else if(instr_arr[1]=="1024B")

{

if(instr_arr[3]=="DWORD")
{encoded="611248";}

else if(instr_arr[3]=="QWORD")
{encoded="611268";}

}

else if(instr_arr[1]=="2048B")

{

if(instr_arr[3]=="DWORD")
{encoded="611249";}

else if(instr_arr[3]=="QWORD")
{encoded="611269";}

}

else if(instr_arr[1]=="4096B")

{

if(instr_arr[3]=="DWORD")
{encoded="61124A";}

else if(instr_arr[3]=="QWORD")
{encoded="61126A";}

}


else if(instr_arr[1]=="8192B")

{

if(instr_arr[3]=="DWORD")
{encoded="61124B";}

else if(instr_arr[3]=="QWORD")
{encoded="61126B";}

}


}














/////////end of VPERM///////////////////////////























//bin="";
RVU_Dest_bin=binary(RVU_Dest);
lng=RVU_Dest_bin.length();
zero_str="";

if (lng<5)
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Dest_bin=zero_str+RVU_Dest_bin;
	
	
}
//bin="";	
RVU_Regno_bin=binary(RVU_Regno);

lng=RVU_Regno_bin.length();
zero_str="";

if (lng<14)
{
	counnt=14-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Regno_bin=zero_str+RVU_Regno_bin;
	
	
}



RVU_src1_bin=binary(RVU_src1);
lng=RVU_src1_bin.length();
zero_str="";

if (lng<5)
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_src1_bin=zero_str+RVU_src1_bin;
	
	
}
//bin="";	
RVU_Regno_src1_bin=binary(RVU_Regno_src1);

lng=RVU_Regno_src1_bin.length();
zero_str="";

if (lng<14)
{
	counnt=14-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Regno_src1_bin=zero_str+RVU_Regno_src1_bin;
	
	
}





RVU_src2_bin=binary(RVU_src2);
lng=RVU_src2_bin.length();
zero_str="";

if (lng<5)
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_src2_bin=zero_str+RVU_src2_bin;
	
	
}
//bin="";	
RVU_Regno_src2_bin=binary(RVU_Regno_src2);

lng=RVU_Regno_src2_bin.length();
zero_str="";

if (lng<14)
{
	counnt=14-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Regno_src2_bin=zero_str+RVU_Regno_src2_bin;
	
	
}







bin_fields="";
bin_fields=RVU_Dest_bin+RVU_Regno_bin+RVU_src1_bin+RVU_Regno_src1_bin+RVU_src2_bin+RVU_Regno_src2_bin+"0000000";

hex_fields="";
hex_fields=BintoHex(bin_fields);

//hex_fields.erase(0,2);
encoded=encoded+hex_fields;


 //OS << (char*)encoded.c_str();



 int init=0, final=encoded.length();
 uint8_t ul;
 std::string str="";

 while(init!=final)

 {

	 str="0x"+encoded.substr(init,2);
	  ul = std::stoul (str,nullptr,0);
	  OS << (char)ul;
	 init+=2;

 }






}////////////end of arithmetic


////////////VMOVV Instructions/////////////////

else if(instr_arr[1]=="VMOVV") ////////////use arr[1] because in backend instruction is defined as PIM_VMOVV_.....
    
    
{

int lenggth1=0, i=0;size_t contain;
int lenggth2=0;
int lenggth3=0;
std::string dest_reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(0).getReg()); /////////incase of add 0th operand is destination reg
std::string src1_reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(1).getReg()); ///////////1st operand is source1 reg



while( 1 )
{
    contain = dest_reg_name.find("_", i);
    if (contain != string::npos )
    {
        lenggth1++;
        i = contain + 1;
    }
    else
    {
        break;
    }
}

i=0;
while( 1 )
{
    contain = src1_reg_name.find("_", i);
    if (contain != string::npos )
    {
        lenggth2++;
        i = contain + 1;
    }
    else
    {
        break;
    }
}


reg_arr=parsed(dest_reg_name,"_");


RVU_Dest=std::stoi(reg_arr[1]);

RVU_Regno=std::stoi(reg_arr[lenggth1]);




reg_arr_src1=parsed(src1_reg_name,"_");


RVU_src1=std::stoi(reg_arr_src1[1]);

RVU_Regno_src1=std::stoi(reg_arr_src1[lenggth2]);


if(instr_arr[2]=="32B")

{encoded="611303";}

else if(instr_arr[2]=="64B")

{encoded="611304";}

else if(instr_arr[2]=="128B")

{encoded="611305";}


else if(instr_arr[2]=="256B")

{encoded="611306";}

else if(instr_arr[2]=="512B")

{encoded="611307";}


else if(instr_arr[2]=="1024B")

{encoded="611308";}



else if(instr_arr[2]=="2048B")

{encoded="611309";}



else if(instr_arr[2]=="4096B")

{encoded="61130A";}


else if(instr_arr[2]=="8192B")

{encoded="61130B";}


RVU_Dest_bin=binary(RVU_Dest);
lng=RVU_Dest_bin.length();
zero_str="";

if (lng<5)
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Dest_bin=zero_str+RVU_Dest_bin;
	
	
}
RVU_Regno_bin=binary(RVU_Regno);

lng=RVU_Regno_bin.length();
zero_str="";

if (lng<14)
{
	counnt=14-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Regno_bin=zero_str+RVU_Regno_bin;
	
	
}



RVU_src1_bin=binary(RVU_src1);
lng=RVU_src1_bin.length();
zero_str="";

if (lng<5)
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_src1_bin=zero_str+RVU_src1_bin;
	
	
}
RVU_Regno_src1_bin=binary(RVU_Regno_src1);

lng=RVU_Regno_src1_bin.length();
zero_str="";

if (lng<14)
{
	counnt=14-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Regno_src1_bin=zero_str+RVU_Regno_src1_bin;
	
	
}


bin_fields="";
bin_fields=RVU_Dest_bin+RVU_Regno_bin+RVU_src1_bin+RVU_Regno_src1_bin+"00";

hex_fields="";
hex_fields=BintoHex(bin_fields);

encoded=encoded+hex_fields;

 int init=0, final=encoded.length();
 uint8_t ul;
 std::string str="";

 while(init!=final)

 {

	 str="0x"+encoded.substr(init,2);
	  ul = std::stoul (str,nullptr,0);
	  OS << (char)ul;
	 init+=2;

 }


} //////////end of vmovv





else if(instr_arr[2].substr(0,9)=="VMOVPIMto") 
    
    
{

int lenggth1=0, i=0;size_t contain;
int lenggth2=0;
int lenggth3=0;


string  X86_Dest_bin;

std::string dest_reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(0).getReg()); /////////incase of pimtoymm 0th operand is destination reg
std::string src1_reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(1).getReg()); ///////////1st operand is source1 reg

X86_Dest_bin=Reg_Encoding(dest_reg_name);


while( 1 )
{
    contain = src1_reg_name.find("_", i);
    if (contain != string::npos )
    {
        lenggth2++;
        i = contain + 1;
    }
    else
    {
        break;
    }
}






reg_arr_src1=parsed(src1_reg_name,"_");


RVU_src1=std::stoi(reg_arr_src1[1]);

RVU_Regno_src1=std::stoi(reg_arr_src1[lenggth2]);





if(instr_arr[1]=="16B")

{encoded="611442";}


else if(instr_arr[1]=="32B")

{encoded="611443";}

else if(instr_arr[1]=="64B")

{encoded="611444";}









lng=X86_Dest_bin.length();
zero_str="";

if (lng<5)
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	X86_Dest_bin=zero_str+X86_Dest_bin;
	
	
}


RVU_src1_bin=binary(RVU_src1);
lng=RVU_src1_bin.length();
zero_str="";

if (lng<5)
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_src1_bin=zero_str+RVU_src1_bin;
	
	
}
RVU_Regno_src1_bin=binary(RVU_Regno_src1);

lng=RVU_Regno_src1_bin.length();
zero_str="";

if (lng<14)
{
	counnt=14-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Regno_src1_bin=zero_str+RVU_Regno_src1_bin;
	
	
}


bin_fields="";
bin_fields=X86_Dest_bin+RVU_src1_bin+RVU_Regno_src1_bin;

hex_fields="";
hex_fields=BintoHex(bin_fields);

encoded=encoded+hex_fields;

 int init=0, final=encoded.length();
 uint8_t ul;
 std::string str="";

 while(init!=final)

 {

	 str="0x"+encoded.substr(init,2);
	  ul = std::stoul (str,nullptr,0);
	  OS << (char)ul;
	 init+=2;

 }


} //////////end of vmovpimtoymm





else if(instr_arr[2]=="VMOVYMMtoPIM" || instr_arr[2]=="VMOVXMMtoPIM" || instr_arr[2]=="VMOVZMMtoPIM") 
    
    
{

int lenggth1=0, i=0;size_t contain;
int lenggth2=0;
int lenggth3=0;


string  X86_Dest_bin;

std::string dest_reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(0).getReg()); /////////incase of pimtoymm 0th operand is dest reg
std::string src1_reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(1).getReg()); ///////////1st operand is source reg

X86_Dest_bin=Reg_Encoding(src1_reg_name);


while( 1 )
{
    contain = dest_reg_name.find("_", i);
    if (contain != string::npos )
    {
        lenggth2++;
        i = contain + 1;
    }
    else
    {
        break;
    }
}






reg_arr_src1=parsed(dest_reg_name,"_");


RVU_src1=std::stoi(reg_arr_src1[1]);

RVU_Regno_src1=std::stoi(reg_arr_src1[lenggth2]);





if(instr_arr[1]=="16B")

{encoded="612442";}


else if(instr_arr[1]=="32B")

{encoded="612443";}

else if(instr_arr[1]=="64B")

{encoded="612444";}









lng=X86_Dest_bin.length();
zero_str="";

if (lng<5)
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	X86_Dest_bin=zero_str+X86_Dest_bin;
	
	
}


RVU_src1_bin=binary(RVU_src1);
lng=RVU_src1_bin.length();
zero_str="";

if (lng<5)
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_src1_bin=zero_str+RVU_src1_bin;
	
	
}
RVU_Regno_src1_bin=binary(RVU_Regno_src1);

lng=RVU_Regno_src1_bin.length();
zero_str="";

if (lng<14)
{
	counnt=14-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Regno_src1_bin=zero_str+RVU_Regno_src1_bin;
	
	
}


bin_fields="";
bin_fields=RVU_src1_bin+RVU_Regno_src1_bin+X86_Dest_bin;

hex_fields="";
hex_fields=BintoHex(bin_fields);

encoded=encoded+hex_fields;

 int init=0, final=encoded.length();
 uint8_t ul;
 std::string str="";

 while(init!=final)

 {

	 str="0x"+encoded.substr(init,2);
	  ul = std::stoul (str,nullptr,0);
	  OS << (char)ul;
	 init+=2;

 }


} //////////end of vmovrmmtopim



else if(instr_arr[2]=="VSRLI")
    
    
{

int lenggth1=0, i=0;size_t contain;
int lenggth2=0;
int lenggth3=0;
std::string dest_reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(0).getReg()); /////////incase of add 0th operand is destination reg
std::string src1_reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(1).getReg()); ///////////1st operand is source1 reg

imm_src2=MI.getOperand(2).getImm();
	



while( 1 )
{
    contain = dest_reg_name.find("_", i);
    if (contain != string::npos )
    {
        lenggth1++;
        i = contain + 1;
    }
    else
    {
        break;
    }
}

i=0;
while( 1 )
{
    contain = src1_reg_name.find("_", i);
    if (contain != string::npos )
    {
        lenggth2++;
        i = contain + 1;
    }
    else
    {
        break;
    }
}





reg_arr=parsed(dest_reg_name,"_");


RVU_Dest=std::stoi(reg_arr[1]);

RVU_Regno=std::stoi(reg_arr[lenggth1]);


reg_arr_src1=parsed(src1_reg_name,"_");


RVU_src1=std::stoi(reg_arr_src1[1]);

RVU_Regno_src1=std::stoi(reg_arr_src1[lenggth2]);


if(instr_arr[1]=="128B")

{

if(instr_arr[3]=="DWORD")
{encoded="614A45";}

else if(instr_arr[3]=="QWORD")
{encoded="614A65";}

}



else if(instr_arr[1]=="256B")

{

if(instr_arr[3]=="DWORD")
{encoded="614A46";}

else if(instr_arr[3]=="QWORD")
{encoded="614A66";}

}


else if(instr_arr[1]=="512B")

{

if(instr_arr[3]=="DWORD")
{encoded="614A47";}

else if(instr_arr[3]=="QWORD")
{encoded="614A67";}

}

else if(instr_arr[1]=="1024B")

{

if(instr_arr[3]=="DWORD")
{encoded="614A48";}

else if(instr_arr[3]=="QWORD")
{encoded="614A68";}

}

else if(instr_arr[1]=="2048B")

{

if(instr_arr[3]=="DWORD")
{encoded="614A49";}

else if(instr_arr[3]=="QWORD")
{encoded="614A69";}

}

else if(instr_arr[1]=="4096B")

{

if(instr_arr[3]=="DWORD")
{encoded="614A4A";}

else if(instr_arr[3]=="QWORD")
{encoded="614A6A";}

}


else if(instr_arr[1]=="8192B")

{

if(instr_arr[3]=="DWORD")
{encoded="614A4B";}

else if(instr_arr[3]=="QWORD")
{encoded="614A6B";}

}


RVU_Dest_bin=binary(RVU_Dest);
lng=RVU_Dest_bin.length();
zero_str="";

if (lng<5)
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Dest_bin=zero_str+RVU_Dest_bin;
	
	
}
	
RVU_Regno_bin=binary(RVU_Regno);

lng=RVU_Regno_bin.length();
zero_str="";

if (lng<14)
{
	counnt=14-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Regno_bin=zero_str+RVU_Regno_bin;
	
	
}

RVU_src1_bin=binary(RVU_src1);
lng=RVU_src1_bin.length();
zero_str="";

if (lng<5)
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_src1_bin=zero_str+RVU_src1_bin;	
}

RVU_Regno_src1_bin=binary(RVU_Regno_src1);

lng=RVU_Regno_src1_bin.length();
zero_str="";

if (lng<14)
{
	counnt=14-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Regno_src1_bin=zero_str+RVU_Regno_src1_bin;
	
	
}


imm_src2_bin=binary(imm_src2);

lng=imm_src2_bin.length();
zero_str="";

if (lng<8)
{
	counnt=8-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
imm_src2_bin=zero_str+imm_src2_bin;
	
	
}





bin_fields="";
bin_fields=RVU_Dest_bin+RVU_Regno_bin+RVU_src1_bin+RVU_Regno_src1_bin+"00"+imm_src2_bin;

hex_fields="";
hex_fields=BintoHex(bin_fields);

encoded=encoded+hex_fields;

 int init=0, final=encoded.length();
 uint8_t ul;
 std::string str="";

 while(init!=final)

 {

	 str="0x"+encoded.substr(init,2);
	  ul = std::stoul (str,nullptr,0);
	  OS << (char)ul;
	 init+=2;

 }


}




else if(instr_arr[2]=="GATHER" || instr_arr[2]=="FGATHER")   //////////if upcoming instruction is load instruction
    
    
{



int lenggth=0, i=0;size_t contain; gpr3=""; gpr3_bin="";
reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(0).getReg()); //////////// 0th operand is destination register
gpr=Ctx.getRegisterInfo()->getName(MI.getOperand(1).getReg()); //////////// 1st operand is mask register
gpr2=Ctx.getRegisterInfo()->getName(MI.getOperand(6).getReg()); ////////////6th operand is zmm register
if (MI.getOperand(4).getReg()!=0)
{
gpr3=Ctx.getRegisterInfo()->getName(MI.getOperand(4).getReg());} ////////////4th operand is base register if gather of type2
imm_src2=MI.getOperand(5).getImm(); ////////////5th operand is immediate if gather of type2

while( 1 )   ///////////////count the occurences of _ in reg_name
{
    contain = reg_name.find("_", i);
    if (contain != string::npos )
    {
        lenggth++;
        i = contain + 1;
    }
    else
    {
        break;
    }
}




reg_arr=parsed(reg_name,"_");  ////////////parse reg_name


RVU_Dest=std::stoi(reg_arr[1]);    ////////////the 2nd field of reg_name indicates RVU number

RVU_Regno=std::stoi(reg_arr[lenggth]);



if (gpr3!="" && imm_src2>1) /////////type 2 gather

{

if(instr_arr[1]=="32B") ///////////for setting the opcodes accordingly

{

if(instr_arr[2]=="GATHER")

{


if(instr_arr[3]=="QD")
{encoded="613943";}

}


else if(instr_arr[2]=="FGATHER")

{


if(instr_arr[3]=="QD")
{encoded="613943";}

}


}



else if(instr_arr[1]=="64B") ///////////for setting the opcodes accordingly

{

if(instr_arr[2]=="GATHER")

{

if(instr_arr[3]=="DD")
{encoded="613844";}

else if(instr_arr[3]=="DQ")
{encoded="613864";}

else if(instr_arr[3]=="QQ")
{encoded="613964";}

}


else if(instr_arr[2]=="FGATHER")

{

if(instr_arr[3]=="DD")
{encoded="613844";}

else if(instr_arr[3]=="DQ")
{encoded="613864";}

else if(instr_arr[3]=="QQ")
{encoded="613964";}


}

}

} ///////////end of type 2 gather if




else{



if(instr_arr[1]=="32B") ///////////for setting the opcodes accordingly

{

if(instr_arr[2]=="GATHER")

{


if(instr_arr[3]=="QD")
{encoded="611943";}

}


else if(instr_arr[2]=="FGATHER")

{


if(instr_arr[3]=="QD")
{encoded="611943";}

}


}



else if(instr_arr[1]=="64B") ///////////for setting the opcodes accordingly

{

if(instr_arr[2]=="GATHER")

{

if(instr_arr[3]=="DD")
{encoded="611844";}

else if(instr_arr[3]=="DQ")
{encoded="611864";}

else if(instr_arr[3]=="QQ")
{encoded="611964";}

}


else if(instr_arr[2]=="FGATHER")

{

if(instr_arr[3]=="DD")
{encoded="611844";}

else if(instr_arr[3]=="DQ")
{encoded="611864";}

else if(instr_arr[3]=="QQ")
{encoded="611964";}


}

}



else if(instr_arr[1]=="128B") ///////////for setting the opcodes accordingly

{

if(instr_arr[2]=="FGATHER")

{
if(instr_arr[3]=="DQ")
{encoded="611865";}

}

else if(instr_arr[2]=="GATHER")

{
if(instr_arr[3]=="DQ")
{encoded="611865";}
}


}

} //////////////end of gather type else

RVU_Dest_bin=binary(RVU_Dest); ////////////convert the RVU no into binary
lng=RVU_Dest_bin.length();
zero_str="";

if (lng<5) ////////////append required no of 0s
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Dest_bin=zero_str+RVU_Dest_bin;
	
	
}
RVU_Regno_bin=binary(RVU_Regno);

lng=RVU_Regno_bin.length();
zero_str="";

if (lng<14)
{
	counnt=14-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Regno_bin=zero_str+RVU_Regno_bin;
	
	
}

gpr_bin=Reg_Encoding(gpr);
gpr2_bin=Reg_Encoding(gpr2);

if(gpr3!="")
{
gpr3_bin=Reg_Encoding(gpr3);
}

if(imm_src2>1)
{
imm_src2_bin=binary(imm_src2);

lng=imm_src2_bin.length();
zero_str="";

if (lng<32)
{
	counnt=32-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
imm_src2_bin=zero_str+imm_src2_bin;
	
	
}

}



bin_fields="";


/////////////////

if(gpr3!="" && imm_src2>1)

{
bin_fields=RVU_Dest_bin+RVU_Regno_bin+gpr_bin+gpr2_bin+gpr3_bin+"000000"+imm_src2_bin;  ////////////append all the encoded fields in 1

}

else{


bin_fields=RVU_Dest_bin+RVU_Regno_bin+gpr_bin+gpr2_bin+"0000";  ////////////append all the encoded fields in 1
}

	

hex_fields="";
hex_fields=BintoHex(bin_fields);  ////////////convert to hex

//hex_fields.erase(0,2);  /////////////remove 0x
encoded=encoded+hex_fields;  ///////////////append opcode in encoded string

int init=0, final=encoded.length();
 uint8_t ul;
 std::string str="";

 while(init!=final)

 {

	 str="0x"+encoded.substr(init,2);
	  ul = std::stoul (str,nullptr,0);
	  OS << (char)ul;
	 init+=2;

 }



}



else if(instr_arr[2]=="SCATTER" || instr_arr[2]=="FSCATTER")   //////////if upcoming instruction is load instruction
    
    
{



int lenggth=0, i=0;size_t contain;
reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(7).getReg()); //////////// 7th operand is source register
gpr=Ctx.getRegisterInfo()->getName(MI.getOperand(0).getReg()); //////////// 0th operand is mask register
gpr2=Ctx.getRegisterInfo()->getName(MI.getOperand(3).getReg()); ////////////3rd operand is zmm register


while( 1 )   ///////////////count the occurences of _ in reg_name
{
    contain = reg_name.find("_", i);
    if (contain != string::npos )
    {
        lenggth++;
        i = contain + 1;
    }
    else
    {
        break;
    }
}




reg_arr=parsed(reg_name,"_");  ////////////parse reg_name


RVU_Dest=std::stoi(reg_arr[1]);    ////////////the 2nd field of reg_name indicates RVU number

RVU_Regno=std::stoi(reg_arr[lenggth]);



if(instr_arr[1]=="32B") ///////////for setting the opcodes accordingly

{

if(instr_arr[2]=="SCATTER")

{


if(instr_arr[3]=="QD")
{encoded="611B43";}

}


else if(instr_arr[2]=="FSCATTER")

{


if(instr_arr[3]=="QD")
{encoded="611B43";}

}


}



else if(instr_arr[1]=="64B") ///////////for setting the opcodes accordingly

{

if(instr_arr[2]=="SCATTER")

{

if(instr_arr[3]=="DD")
{encoded="611A44";}

else if(instr_arr[3]=="DQ")
{encoded="611A64";}

else if(instr_arr[3]=="QQ")
{encoded="611B64";}

}


else if(instr_arr[2]=="FSCATTER")

{

if(instr_arr[3]=="DD")
{encoded="611A44";}

else if(instr_arr[3]=="DQ")
{encoded="611A64";}

else if(instr_arr[3]=="QQ")
{encoded="611B64";}


}

}



else if(instr_arr[1]=="128B") ///////////for setting the opcodes accordingly

{

if(instr_arr[2]=="FSCATTER")

{
if(instr_arr[3]=="DQ")
{encoded="611A65";}

}

else if(instr_arr[2]=="SCATTER")

{
if(instr_arr[3]=="DQ")
{encoded="611A65";}
}


}

RVU_Dest_bin=binary(RVU_Dest); ////////////convert the RVU no into binary
lng=RVU_Dest_bin.length();
zero_str="";

if (lng<5) ////////////append required no of 0s
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Dest_bin=zero_str+RVU_Dest_bin;
	
	
}
RVU_Regno_bin=binary(RVU_Regno);

lng=RVU_Regno_bin.length();
zero_str="";

if (lng<14)
{
	counnt=14-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Regno_bin=zero_str+RVU_Regno_bin;
	
	
}

gpr_bin=Reg_Encoding(gpr);
gpr2_bin=Reg_Encoding(gpr2);
bin_fields="";


/////////////////

bin_fields=gpr2_bin+gpr_bin+RVU_Dest_bin+RVU_Regno_bin+"0000";  ////////////append all the encoded fields in 1


	

hex_fields="";
hex_fields=BintoHex(bin_fields);  ////////////convert to hex

//hex_fields.erase(0,2);  /////////////remove 0x
encoded=encoded+hex_fields;  ///////////////append opcode in encoded string

int init=0, final=encoded.length();
 uint8_t ul;
 std::string str="";

 while(init!=final)

 {

	 str="0x"+encoded.substr(init,2);
	  ul = std::stoul (str,nullptr,0);
	  OS << (char)ul;
	 init+=2;

 }



}


















else if(instr_arr[2]=="VSHUF32x4" || instr_arr[2]=="VSHUF64x2" || instr_arr[2]=="PSHUFFLE"  || instr_arr[2]=="VINSERTi64x4" || instr_arr[2]=="VINSERTf64x4" || instr_arr[2]=="VEXTRACTixHALF" || instr_arr[2]=="VEXTRACTfxHALF")   //////////if upcoming instruction is load instruction
    
    
{



int lenggth1=0, i=0;size_t contain;
int lenggth2=0;
int lenggth3=0;
std::string dest_reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(0).getReg()); /////////incase of add 0th operand is destination reg
std::string src1_reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(1).getReg()); ///////////1st operand is source1 reg
std::string src2_reg_name;
if(instr_arr[2]=="VSHUF32x4" || instr_arr[2]=="VSHUF64x2"|| instr_arr[2]=="VINSERTi64x4" || instr_arr[2]=="VINSERTf64x4")
{src2_reg_name=Ctx.getRegisterInfo()->getName(MI.getOperand(2).getReg());  

imm_src2=MI.getOperand(3).getImm();
}

/////////////////2nd operand is source2 reg}

else if (instr_arr[2]=="PSHUFFLE" || instr_arr[2]=="VEXTRACTixHALF" || instr_arr[2]=="VEXTRACTfxHALF")

{
imm_src2=MI.getOperand(2).getImm();

}



while( 1 )
{
    contain = dest_reg_name.find("_", i);
    if (contain != string::npos )
    {
        lenggth1++;
        i = contain + 1;
    }
    else
    {
        break;
    }
}

i=0;
while( 1 )
{
    contain = src1_reg_name.find("_", i);
    if (contain != string::npos )
    {
        lenggth2++;
        i = contain + 1;
    }
    else
    {
        break;
    }
}

if(instr_arr[2]=="VSHUF32x4" || instr_arr[2]=="VSHUF64x2"|| instr_arr[2]=="VINSERTi64x4" || instr_arr[2]=="VINSERTf64x4"){
i=0;
while( 1 )
{
    contain = src2_reg_name.find("_", i);
    if (contain != string::npos )
    {
        lenggth3++;
        i = contain + 1;
    }
    else
    {
        break;
    }
}


}

reg_arr=parsed(dest_reg_name,"_");


RVU_Dest=std::stoi(reg_arr[1]);

RVU_Regno=std::stoi(reg_arr[lenggth1]);




reg_arr_src1=parsed(src1_reg_name,"_");


RVU_src1=std::stoi(reg_arr_src1[1]);

RVU_Regno_src1=std::stoi(reg_arr_src1[lenggth2]);


if(instr_arr[2]=="VSHUF32x4" || instr_arr[2]=="VSHUF64x2"|| instr_arr[2]=="VINSERTi64x4" || instr_arr[2]=="VINSERTf64x4"){
reg_arr_src2=parsed(src2_reg_name,"_");


RVU_src2=std::stoi(reg_arr_src2[1]);

RVU_Regno_src2=std::stoi(reg_arr_src2[lenggth3]);

}





if(instr_arr[1]=="64B") ///////////for setting the opcodes accordingly

{

if(instr_arr[2]=="VSHUF32x4")

{


if(instr_arr[3]=="DWORD")
{encoded="611544";}

}


else if(instr_arr[2]=="VSHUF64x2")

{


if(instr_arr[3]=="QWORD")
{encoded="611664";}

}


else if(instr_arr[2]=="PSHUFFLE")

{


if(instr_arr[3]=="DWORD")
{encoded="611C44";}


else if(instr_arr[3]=="QWORD")
{encoded="611C64";}

}


else if(instr_arr[2]=="VINSERTi64x4")

{


if(instr_arr[3]=="DWORD")
{encoded="617C44";}


}


else if(instr_arr[2]=="VINSERTf64x4")

{


if(instr_arr[3]=="DWORD")
{encoded="617844";}


}




else if(instr_arr[2]=="VEXTRACTixHALF")

{


if(instr_arr[3]=="DWORD")
{encoded="617F44";}


else if(instr_arr[3]=="QWORD")
{encoded="617F64";}

}


else if(instr_arr[2]=="VEXTRACTfxHALF")

{


if(instr_arr[3]=="DWORD")
{encoded="617B44";}


else if(instr_arr[3]=="QWORD")
{encoded="617B64";}

}


} ///end of 64B


if(instr_arr[1]=="128B") ///////////for setting the opcodes accordingly

{

if(instr_arr[2]=="VSHUF32x4")

{


if(instr_arr[3]=="DWORD")
{encoded="611545";}

}


else if(instr_arr[2]=="VSHUF64x2")

{


if(instr_arr[3]=="QWORD")
{encoded="611665";}

}


else if(instr_arr[2]=="PSHUFFLE")

{


if(instr_arr[3]=="DWORD")
{encoded="611C45";}


else if(instr_arr[3]=="QWORD")
{encoded="611C65";}

}


else if(instr_arr[2]=="VINSERTi64x4")

{


if(instr_arr[3]=="DWORD")
{encoded="617C45";}


}


else if(instr_arr[2]=="VINSERTf64x4")

{


if(instr_arr[3]=="DWORD")
{encoded="617845";}


}



else if(instr_arr[2]=="VEXTRACTixHALF")

{


if(instr_arr[3]=="DWORD")
{encoded="617F45";}


else if(instr_arr[3]=="QWORD")
{encoded="617F65";}

}


else if(instr_arr[2]=="VEXTRACTfxHALF")

{


if(instr_arr[3]=="DWORD")
{encoded="617B45";}


else if(instr_arr[3]=="QWORD")
{encoded="617B65";}

}


} //end of 128B


else if(instr_arr[1]=="256B") ///////////for setting the opcodes accordingly

{

if(instr_arr[2]=="VSHUF32x4")

{


if(instr_arr[3]=="DWORD")
{encoded="611546";}

}


else if(instr_arr[2]=="VSHUF64x2")

{


if(instr_arr[3]=="QWORD")
{encoded="611666";}

}


else if(instr_arr[2]=="PSHUFFLE")

{


if(instr_arr[3]=="DWORD")
{encoded="611C46";}


else if(instr_arr[3]=="QWORD")
{encoded="611C66";}

}


else if(instr_arr[2]=="VINSERTi64x4")

{


if(instr_arr[3]=="DWORD")
{encoded="617C46";}


}


else if(instr_arr[2]=="VINSERTf64x4")

{


if(instr_arr[3]=="DWORD")
{encoded="617846";}


}







else if(instr_arr[2]=="VEXTRACTixHALF")

{


if(instr_arr[3]=="DWORD")
{encoded="617F46";}


else if(instr_arr[3]=="QWORD")
{encoded="617F66";}

}


else if(instr_arr[2]=="VEXTRACTfxHALF")

{


if(instr_arr[3]=="DWORD")
{encoded="617B46";}


else if(instr_arr[3]=="QWORD")
{encoded="617B66";}

}



} //end of 256B


else if(instr_arr[1]=="512B") ///////////for setting the opcodes accordingly

{






if(instr_arr[2]=="VEXTRACTixHALF")

{


if(instr_arr[3]=="DWORD")
{encoded="617F47";}


else if(instr_arr[3]=="QWORD")
{encoded="617F67";}

}


else if(instr_arr[2]=="VEXTRACTfxHALF")

{


if(instr_arr[3]=="DWORD")
{encoded="617B47";}


else if(instr_arr[3]=="QWORD")
{encoded="617B67";}

}

else if(instr_arr[2]=="VINSERTi64x4")

{


if(instr_arr[3]=="DWORD")
{encoded="617C47";}


}


else if(instr_arr[2]=="VINSERTf64x4")

{


if(instr_arr[3]=="DWORD")
{encoded="617847";}


}



} //end of 512B



else if(instr_arr[1]=="1024B") ///////////for setting the opcodes accordingly

{






if(instr_arr[2]=="VEXTRACTixHALF")

{


if(instr_arr[3]=="DWORD")
{encoded="617F48";}


else if(instr_arr[3]=="QWORD")
{encoded="617F68";}

}


else if(instr_arr[2]=="VEXTRACTfxHALF")

{


if(instr_arr[3]=="DWORD")
{encoded="617B48";}


else if(instr_arr[3]=="QWORD")
{encoded="617B68";}

}


else if(instr_arr[2]=="VINSERTi64x4")

{


if(instr_arr[3]=="DWORD")
{encoded="617C48";}


}


else if(instr_arr[2]=="VINSERTf64x4")

{


if(instr_arr[3]=="DWORD")
{encoded="617848";}


}




} //end of 1024B



else if(instr_arr[1]=="2048B") ///////////for setting the opcodes accordingly

{







if(instr_arr[2]=="VEXTRACTixHALF")

{


if(instr_arr[3]=="DWORD")
{encoded="617F49";}


else if(instr_arr[3]=="QWORD")
{encoded="617F69";}

}

else if(instr_arr[2]=="VEXTRACTfxHALF")

{


if(instr_arr[3]=="DWORD")
{encoded="617B49";}


else if(instr_arr[3]=="QWORD")
{encoded="617B69";}

}



else if(instr_arr[2]=="VINSERTi64x4")

{


if(instr_arr[3]=="DWORD")
{encoded="617C49";}


}


else if(instr_arr[2]=="VINSERTf64x4")

{


if(instr_arr[3]=="DWORD")
{encoded="617849";}


}


} //end of 2048B


else if(instr_arr[1]=="4096B") ///////////for setting the opcodes accordingly

{





if(instr_arr[2]=="VEXTRACTixHALF")

{


if(instr_arr[3]=="DWORD")
{encoded="617F4A";}


else if(instr_arr[3]=="QWORD")
{encoded="617F6A";}

}

else if(instr_arr[2]=="VEXTRACTfxHALF")

{


if(instr_arr[3]=="DWORD")
{encoded="617B4A";}


else if(instr_arr[3]=="QWORD")
{encoded="617B6A";}

}


else if(instr_arr[2]=="VINSERTi64x4")

{


if(instr_arr[3]=="DWORD")
{encoded="617C4A";}


}


else if(instr_arr[2]=="VINSERTf64x4")

{


if(instr_arr[3]=="DWORD")
{encoded="61784A";}


}












} //end of 4096B


else if(instr_arr[1]=="8192B") ///////////for setting the opcodes accordingly

{









if(instr_arr[2]=="VEXTRACTixHALF")

{


if(instr_arr[3]=="DWORD")
{encoded="617F4B";}


else if(instr_arr[3]=="QWORD")
{encoded="617F6B";}

}

else if(instr_arr[2]=="VEXTRACTfxHALF")

{


if(instr_arr[3]=="DWORD")
{encoded="617B4B";}


else if(instr_arr[3]=="QWORD")
{encoded="617B6B";}

}


else if(instr_arr[2]=="VINSERTi64x4")

{


if(instr_arr[3]=="DWORD")
{encoded="617C4B";}


}


else if(instr_arr[2]=="VINSERTf64x4")

{


if(instr_arr[3]=="DWORD")
{encoded="61784B";}


}










} //end of 8192B







RVU_Dest_bin=binary(RVU_Dest);
lng=RVU_Dest_bin.length();
zero_str="";

if (lng<5)
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Dest_bin=zero_str+RVU_Dest_bin;
	
	
}
//bin="";	
RVU_Regno_bin=binary(RVU_Regno);

lng=RVU_Regno_bin.length();
zero_str="";

if (lng<14)
{
	counnt=14-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Regno_bin=zero_str+RVU_Regno_bin;
	
	
}



RVU_src1_bin=binary(RVU_src1);
lng=RVU_src1_bin.length();
zero_str="";

if (lng<5)
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_src1_bin=zero_str+RVU_src1_bin;
	
	
}
//bin="";	
RVU_Regno_src1_bin=binary(RVU_Regno_src1);

lng=RVU_Regno_src1_bin.length();
zero_str="";

if (lng<14)
{
	counnt=14-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Regno_src1_bin=zero_str+RVU_Regno_src1_bin;
	
	
}



if(instr_arr[2]=="VSHUF32x4" || instr_arr[2]=="VSHUF64x2"|| instr_arr[2]=="VINSERTi64x4" || instr_arr[2]=="VINSERTf64x4"){

RVU_src2_bin=binary(RVU_src2);
lng=RVU_src2_bin.length();
zero_str="";

if (lng<5)
{
	counnt=5-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_src2_bin=zero_str+RVU_src2_bin;
	
	
}
//bin="";	
RVU_Regno_src2_bin=binary(RVU_Regno_src2);

lng=RVU_Regno_src2_bin.length();
zero_str="";

if (lng<14)
{
	counnt=14-lng;
	for(int i=0; i<counnt;i++)
	{
		zero_str.append("0");
	}
	
	
	RVU_Regno_src2_bin=zero_str+RVU_Regno_src2_bin;
	
	
}

}

imm_src2_bin=decimal_to_binary(imm_src2);

lng=imm_src2_bin.length();

zero_str="";
int imm_lenggth;


if(instr_arr[2]=="VINSERTi64x4" || instr_arr[2]=="VINSERTf64x4" || instr_arr[2]=="VEXTRACTixHALF" || instr_arr[2]=="VEXTRACTfxHALF")

{
imm_lenggth=8;

}


else


{

imm_lenggth=64;

}




if (lng<imm_lenggth)
{
	counnt=imm_lenggth-lng;
	for(int i=0; i<counnt;i++)
	{

                if(imm_src2>=0)
                 {
                     zero_str.append("0");

                   }

                 else if(imm_src2<0) ////////////////for negative numbers

		{zero_str.append("1");}

	}
	
	
imm_src2_bin=zero_str+imm_src2_bin;
	
	
}



bin_fields="";

if(instr_arr[2]=="VSHUF32x4" || instr_arr[2]=="VSHUF64x2"|| instr_arr[2]=="VINSERTi64x4" || instr_arr[2]=="VINSERTf64x4")
{

bin_fields=RVU_Dest_bin+RVU_Regno_bin+RVU_src1_bin+RVU_Regno_src1_bin+RVU_src2_bin+RVU_Regno_src2_bin+"0000000"+imm_src2_bin;}


else if(instr_arr[2]=="PSHUFFLE" || instr_arr[2]=="VEXTRACTixHALF" || instr_arr[2]=="VEXTRACTfxHALF")
{

bin_fields=RVU_Dest_bin+RVU_Regno_bin+RVU_src1_bin+RVU_Regno_src1_bin+"00"+imm_src2_bin;}


hex_fields="";
hex_fields=BintoHex(bin_fields);

//hex_fields.erase(0,2);
encoded=encoded+hex_fields;


 //OS << (char*)encoded.c_str();



 int init=0, final=encoded.length();
 uint8_t ul;
 std::string str="";

 while(init!=final)

 {

	 str="0x"+encoded.substr(init,2);
	  ul = std::stoul (str,nullptr,0);
	  OS << (char)ul;
	 init+=2;

 }

}

















} ////end of function encodepim





void X86MCCodeEmitter::
encodeInstruction(const MCInst &MI, raw_ostream &OS,
                  SmallVectorImpl<MCFixup> &Fixups,
                  const MCSubtargetInfo &STI) const {
  unsigned Opcode = MI.getOpcode();
  
  if (strncmp(MCII.getName(Opcode).str().c_str(), "PIM",3) == 0)  ////////////if the instruction is of PIM
  	  
  	  {
  	  //	  const MCRegisterInfo &MRI;
  	  	  
  	  	  encodePIM(MI, OS, Fixups,STI); //////////call this function
  	  	  
  	  	  
  	  }
  
  
  
  
  
  
  else
  {
  
  const MCInstrDesc &Desc = MCII.get(Opcode);
  uint64_t TSFlags = Desc.TSFlags;

  // Pseudo instructions don't get encoded.
  if ((TSFlags & X86II::FormMask) == X86II::Pseudo)
    return;

  unsigned NumOps = Desc.getNumOperands();
  unsigned CurOp = X86II::getOperandBias(Desc);

  // Keep track of the current byte being emitted.
  unsigned CurByte = 0;

  // Encoding type for this instruction.
  uint64_t Encoding = TSFlags & X86II::EncodingMask;

  // It uses the VEX.VVVV field?
  bool HasVEX_4V = TSFlags & X86II::VEX_4V;
  bool HasVEX_I8Reg = (TSFlags & X86II::ImmMask) == X86II::Imm8Reg;

  // It uses the EVEX.aaa field?
  bool HasEVEX_K = TSFlags & X86II::EVEX_K;
  bool HasEVEX_RC = TSFlags & X86II::EVEX_RC;

  // Used if a register is encoded in 7:4 of immediate.
  unsigned I8RegNum = 0;

  // Determine where the memory operand starts, if present.
  int MemoryOperand = X86II::getMemoryOperandNo(TSFlags);
  if (MemoryOperand != -1) MemoryOperand += CurOp;

  // Emit segment override opcode prefix as needed.
  if (MemoryOperand >= 0)
    EmitSegmentOverridePrefix(CurByte, MemoryOperand+X86::AddrSegmentReg,
                              MI, OS);

  // Emit the repeat opcode prefix as needed.
  if (TSFlags & X86II::REP)
    EmitByte(0xF3, CurByte, OS);

  // Emit the address size opcode prefix as needed.
  bool need_address_override;
  uint64_t AdSize = TSFlags & X86II::AdSizeMask;
  if ((is16BitMode(STI) && AdSize == X86II::AdSize32) ||
      (is32BitMode(STI) && AdSize == X86II::AdSize16) ||
      (is64BitMode(STI) && AdSize == X86II::AdSize32)) {
    need_address_override = true;
  } else if (MemoryOperand < 0) {
    need_address_override = false;
  } else if (is64BitMode(STI)) {
    assert(!Is16BitMemOperand(MI, MemoryOperand, STI));
    need_address_override = Is32BitMemOperand(MI, MemoryOperand);
  } else if (is32BitMode(STI)) {
    assert(!Is64BitMemOperand(MI, MemoryOperand));
    need_address_override = Is16BitMemOperand(MI, MemoryOperand, STI);
  } else {
    assert(is16BitMode(STI));
    assert(!Is64BitMemOperand(MI, MemoryOperand));
    need_address_override = !Is16BitMemOperand(MI, MemoryOperand, STI);
  }

  if (need_address_override)
    EmitByte(0x67, CurByte, OS);

  bool Rex = false;
  if (Encoding == 0)
    Rex = emitOpcodePrefix(TSFlags, CurByte, MemoryOperand, MI, Desc, STI, OS);
  else
    EmitVEXOpcodePrefix(TSFlags, CurByte, MemoryOperand, MI, Desc, OS);

  uint8_t BaseOpcode = X86II::getBaseOpcodeFor(TSFlags);

  if (TSFlags & X86II::Has3DNow0F0FOpcode)
    BaseOpcode = 0x0F;   // Weird 3DNow! encoding.

  uint64_t Form = TSFlags & X86II::FormMask;
  switch (Form) {
  default: errs() << "FORM: " << Form << "\n";
    llvm_unreachable("Unknown FormMask value in X86MCCodeEmitter!");
  case X86II::Pseudo:
    llvm_unreachable("Pseudo instruction shouldn't be emitted");
  case X86II::RawFrmDstSrc: {
    unsigned siReg = MI.getOperand(1).getReg();
    assert(((siReg == X86::SI && MI.getOperand(0).getReg() == X86::DI) ||
            (siReg == X86::ESI && MI.getOperand(0).getReg() == X86::EDI) ||
            (siReg == X86::RSI && MI.getOperand(0).getReg() == X86::RDI)) &&
           "SI and DI register sizes do not match");
    // Emit segment override opcode prefix as needed (not for %ds).
    if (MI.getOperand(2).getReg() != X86::DS)
      EmitSegmentOverridePrefix(CurByte, 2, MI, OS);
    // Emit AdSize prefix as needed.
    if ((!is32BitMode(STI) && siReg == X86::ESI) ||
        (is32BitMode(STI) && siReg == X86::SI))
      EmitByte(0x67, CurByte, OS);
    CurOp += 3; // Consume operands.
    EmitByte(BaseOpcode, CurByte, OS);
    break;
  }
  case X86II::RawFrmSrc: {
    unsigned siReg = MI.getOperand(0).getReg();
    // Emit segment override opcode prefix as needed (not for %ds).
    if (MI.getOperand(1).getReg() != X86::DS)
      EmitSegmentOverridePrefix(CurByte, 1, MI, OS);
    // Emit AdSize prefix as needed.
    if ((!is32BitMode(STI) && siReg == X86::ESI) ||
        (is32BitMode(STI) && siReg == X86::SI))
      EmitByte(0x67, CurByte, OS);
    CurOp += 2; // Consume operands.
    EmitByte(BaseOpcode, CurByte, OS);
    break;
  }
  case X86II::RawFrmDst: {
    unsigned siReg = MI.getOperand(0).getReg();
    // Emit AdSize prefix as needed.
    if ((!is32BitMode(STI) && siReg == X86::EDI) ||
        (is32BitMode(STI) && siReg == X86::DI))
      EmitByte(0x67, CurByte, OS);
    ++CurOp; // Consume operand.
    EmitByte(BaseOpcode, CurByte, OS);
    break;
  }
  case X86II::RawFrm:
    EmitByte(BaseOpcode, CurByte, OS);
    break;
  case X86II::RawFrmMemOffs:
    // Emit segment override opcode prefix as needed.
    EmitSegmentOverridePrefix(CurByte, 1, MI, OS);
    EmitByte(BaseOpcode, CurByte, OS);
    EmitImmediate(MI.getOperand(CurOp++), MI.getLoc(),
                  X86II::getSizeOfImm(TSFlags), getImmFixupKind(TSFlags),
                  CurByte, OS, Fixups);
    ++CurOp; // skip segment operand
    break;
  case X86II::RawFrmImm8:
    EmitByte(BaseOpcode, CurByte, OS);
    EmitImmediate(MI.getOperand(CurOp++), MI.getLoc(),
                  X86II::getSizeOfImm(TSFlags), getImmFixupKind(TSFlags),
                  CurByte, OS, Fixups);
    EmitImmediate(MI.getOperand(CurOp++), MI.getLoc(), 1, FK_Data_1, CurByte,
                  OS, Fixups);
    break;
  case X86II::RawFrmImm16:
    EmitByte(BaseOpcode, CurByte, OS);
    EmitImmediate(MI.getOperand(CurOp++), MI.getLoc(),
                  X86II::getSizeOfImm(TSFlags), getImmFixupKind(TSFlags),
                  CurByte, OS, Fixups);
    EmitImmediate(MI.getOperand(CurOp++), MI.getLoc(), 2, FK_Data_2, CurByte,
                  OS, Fixups);
    break;

  case X86II::AddRegFrm:
    EmitByte(BaseOpcode + GetX86RegNum(MI.getOperand(CurOp++)), CurByte, OS);
    break;

  case X86II::MRMDestReg: {
    EmitByte(BaseOpcode, CurByte, OS);
    unsigned SrcRegNum = CurOp + 1;

    if (HasEVEX_K) // Skip writemask
      ++SrcRegNum;

    if (HasVEX_4V) // Skip 1st src (which is encoded in VEX_VVVV)
      ++SrcRegNum;

    EmitRegModRMByte(MI.getOperand(CurOp),
                     GetX86RegNum(MI.getOperand(SrcRegNum)), CurByte, OS);
    CurOp = SrcRegNum + 1;
    break;
  }
  case X86II::MRMDestMem: {
    EmitByte(BaseOpcode, CurByte, OS);
    unsigned SrcRegNum = CurOp + X86::AddrNumOperands;

    if (HasEVEX_K) // Skip writemask
      ++SrcRegNum;

    if (HasVEX_4V) // Skip 1st src (which is encoded in VEX_VVVV)
      ++SrcRegNum;

    emitMemModRMByte(MI, CurOp, GetX86RegNum(MI.getOperand(SrcRegNum)), TSFlags,
                     Rex, CurByte, OS, Fixups, STI);
    CurOp = SrcRegNum + 1;
    break;
  }
  case X86II::MRMSrcReg: {
    EmitByte(BaseOpcode, CurByte, OS);
    unsigned SrcRegNum = CurOp + 1;

    if (HasEVEX_K) // Skip writemask
      ++SrcRegNum;

    if (HasVEX_4V) // Skip 1st src (which is encoded in VEX_VVVV)
      ++SrcRegNum;

    EmitRegModRMByte(MI.getOperand(SrcRegNum),
                     GetX86RegNum(MI.getOperand(CurOp)), CurByte, OS);
    CurOp = SrcRegNum + 1;
    if (HasVEX_I8Reg)
      I8RegNum = getX86RegEncoding(MI, CurOp++);
    // do not count the rounding control operand
    if (HasEVEX_RC)
      --NumOps;
    break;
  }
  case X86II::MRMSrcReg4VOp3: {
    EmitByte(BaseOpcode, CurByte, OS);
    unsigned SrcRegNum = CurOp + 1;

    EmitRegModRMByte(MI.getOperand(SrcRegNum),
                     GetX86RegNum(MI.getOperand(CurOp)), CurByte, OS);
    CurOp = SrcRegNum + 1;
    ++CurOp; // Encoded in VEX.VVVV
    break;
  }
  case X86II::MRMSrcRegOp4: {
    EmitByte(BaseOpcode, CurByte, OS);
    unsigned SrcRegNum = CurOp + 1;

    // Skip 1st src (which is encoded in VEX_VVVV)
    ++SrcRegNum;

    // Capture 2nd src (which is encoded in Imm[7:4])
    assert(HasVEX_I8Reg && "MRMSrcRegOp4 should imply VEX_I8Reg");
    I8RegNum = getX86RegEncoding(MI, SrcRegNum++);

    EmitRegModRMByte(MI.getOperand(SrcRegNum),
                     GetX86RegNum(MI.getOperand(CurOp)), CurByte, OS);
    CurOp = SrcRegNum + 1;
    break;
  }
  case X86II::MRMSrcMem: {
    unsigned FirstMemOp = CurOp+1;

    if (HasEVEX_K) // Skip writemask
      ++FirstMemOp;

    if (HasVEX_4V)
      ++FirstMemOp;  // Skip the register source (which is encoded in VEX_VVVV).

    EmitByte(BaseOpcode, CurByte, OS);

    emitMemModRMByte(MI, FirstMemOp, GetX86RegNum(MI.getOperand(CurOp)),
                     TSFlags, Rex, CurByte, OS, Fixups, STI);
    CurOp = FirstMemOp + X86::AddrNumOperands;
    if (HasVEX_I8Reg)
      I8RegNum = getX86RegEncoding(MI, CurOp++);
    break;
  }
  case X86II::MRMSrcMem4VOp3: {
    unsigned FirstMemOp = CurOp+1;

    EmitByte(BaseOpcode, CurByte, OS);

    emitMemModRMByte(MI, FirstMemOp, GetX86RegNum(MI.getOperand(CurOp)),
                     TSFlags, Rex, CurByte, OS, Fixups, STI);
    CurOp = FirstMemOp + X86::AddrNumOperands;
    ++CurOp; // Encoded in VEX.VVVV.
    break;
  }
  case X86II::MRMSrcMemOp4: {
    unsigned FirstMemOp = CurOp+1;

    ++FirstMemOp;  // Skip the register source (which is encoded in VEX_VVVV).

    // Capture second register source (encoded in Imm[7:4])
    assert(HasVEX_I8Reg && "MRMSrcRegOp4 should imply VEX_I8Reg");
    I8RegNum = getX86RegEncoding(MI, FirstMemOp++);

    EmitByte(BaseOpcode, CurByte, OS);

    emitMemModRMByte(MI, FirstMemOp, GetX86RegNum(MI.getOperand(CurOp)),
                     TSFlags, Rex, CurByte, OS, Fixups, STI);
    CurOp = FirstMemOp + X86::AddrNumOperands;
    break;
  }

  case X86II::MRMXr:
  case X86II::MRM0r: case X86II::MRM1r:
  case X86II::MRM2r: case X86II::MRM3r:
  case X86II::MRM4r: case X86II::MRM5r:
  case X86II::MRM6r: case X86II::MRM7r: {
    if (HasVEX_4V) // Skip the register dst (which is encoded in VEX_VVVV).
      ++CurOp;
    if (HasEVEX_K) // Skip writemask
      ++CurOp;
    EmitByte(BaseOpcode, CurByte, OS);
    EmitRegModRMByte(MI.getOperand(CurOp++),
                     (Form == X86II::MRMXr) ? 0 : Form-X86II::MRM0r,
                     CurByte, OS);
    break;
  }

  case X86II::MRMXm:
  case X86II::MRM0m: case X86II::MRM1m:
  case X86II::MRM2m: case X86II::MRM3m:
  case X86II::MRM4m: case X86II::MRM5m:
  case X86II::MRM6m: case X86II::MRM7m: {
    if (HasVEX_4V) // Skip the register dst (which is encoded in VEX_VVVV).
      ++CurOp;
    if (HasEVEX_K) // Skip writemask
      ++CurOp;
    EmitByte(BaseOpcode, CurByte, OS);
    emitMemModRMByte(MI, CurOp,
                     (Form == X86II::MRMXm) ? 0 : Form - X86II::MRM0m, TSFlags,
                     Rex, CurByte, OS, Fixups, STI);
    CurOp += X86::AddrNumOperands;
    break;
  }
  case X86II::MRM_C0: case X86II::MRM_C1: case X86II::MRM_C2:
  case X86II::MRM_C3: case X86II::MRM_C4: case X86II::MRM_C5:
  case X86II::MRM_C6: case X86II::MRM_C7: case X86II::MRM_C8:
  case X86II::MRM_C9: case X86II::MRM_CA: case X86II::MRM_CB:
  case X86II::MRM_CC: case X86II::MRM_CD: case X86II::MRM_CE:
  case X86II::MRM_CF: case X86II::MRM_D0: case X86II::MRM_D1:
  case X86II::MRM_D2: case X86II::MRM_D3: case X86II::MRM_D4:
  case X86II::MRM_D5: case X86II::MRM_D6: case X86II::MRM_D7:
  case X86II::MRM_D8: case X86II::MRM_D9: case X86II::MRM_DA:
  case X86II::MRM_DB: case X86II::MRM_DC: case X86II::MRM_DD:
  case X86II::MRM_DE: case X86II::MRM_DF: case X86II::MRM_E0:
  case X86II::MRM_E1: case X86II::MRM_E2: case X86II::MRM_E3:
  case X86II::MRM_E4: case X86II::MRM_E5: case X86II::MRM_E6:
  case X86II::MRM_E7: case X86II::MRM_E8: case X86II::MRM_E9:
  case X86II::MRM_EA: case X86II::MRM_EB: case X86II::MRM_EC:
  case X86II::MRM_ED: case X86II::MRM_EE: case X86II::MRM_EF:
  case X86II::MRM_F0: case X86II::MRM_F1: case X86II::MRM_F2:
  case X86II::MRM_F3: case X86II::MRM_F4: case X86II::MRM_F5:
  case X86II::MRM_F6: case X86II::MRM_F7: case X86II::MRM_F8:
  case X86II::MRM_F9: case X86II::MRM_FA: case X86II::MRM_FB:
  case X86II::MRM_FC: case X86II::MRM_FD: case X86II::MRM_FE:
  case X86II::MRM_FF:
    EmitByte(BaseOpcode, CurByte, OS);
    EmitByte(0xC0 + Form - X86II::MRM_C0, CurByte, OS);
    break;
  }

  if (HasVEX_I8Reg) {
    // The last source register of a 4 operand instruction in AVX is encoded
    // in bits[7:4] of a immediate byte.
    assert(I8RegNum < 16 && "Register encoding out of range");
    I8RegNum <<= 4;
    if (CurOp != NumOps) {
      unsigned Val = MI.getOperand(CurOp++).getImm();
      assert(Val < 16 && "Immediate operand value out of range");
      I8RegNum |= Val;
    }
    EmitImmediate(MCOperand::createImm(I8RegNum), MI.getLoc(), 1, FK_Data_1,
                  CurByte, OS, Fixups);
  } else {
    // If there is a remaining operand, it must be a trailing immediate. Emit it
    // according to the right size for the instruction. Some instructions
    // (SSE4a extrq and insertq) have two trailing immediates.
    while (CurOp != NumOps && NumOps - CurOp <= 2) {
      EmitImmediate(MI.getOperand(CurOp++), MI.getLoc(),
                    X86II::getSizeOfImm(TSFlags), getImmFixupKind(TSFlags),
                    CurByte, OS, Fixups);
    }
  }

  if (TSFlags & X86II::Has3DNow0F0FOpcode)
    EmitByte(X86II::getBaseOpcodeFor(TSFlags), CurByte, OS);

#ifndef NDEBUG
  // FIXME: Verify.
  if (/*!Desc.isVariadic() &&*/ CurOp != NumOps) {
    errs() << "Cannot encode all operands of: ";
    MI.dump();
    errs() << '\n';
    abort();
  }
#endif

}
}
